﻿using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour
{
	public GameObject text;
	public bool isRolled;
	int number;

	void Start ()
	{
		isRolled = false;
	}

	public void diceRolling ()
	{
		if (!isRolled)
		{
			number = Random.Range (1, 7);
			text.GetComponent<Text> ().text = "" + number;
			isRolled = !isRolled;
			Debug.Log ("isRolled is  " + isRolled);
		}
	}

}
