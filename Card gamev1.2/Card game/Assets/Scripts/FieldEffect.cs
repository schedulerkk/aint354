﻿using UnityEngine;
using UnityEngine.UI;

public class FieldEffect : MonoBehaviour
{
	[SerializeField] private Minion.Element element;
	private int randomEle;
	private bool isValueModified;
	private GameObject target;
	private int randomValue;
	private int hpOrAtk;
	private Color origin_color;
	void Start ()
	{
		isValueModified = false;
		origin_color = this.GetComponent<Image> ().color;
		randomEle = Random.Range (1, 6);
		AssignElement (randomEle);
	}

	void Update ()
	{
		//	Change the Element and effect in each 5 rounds, this operate in ButtonManager.EndTurn()

		// Modify the value to the target on this field
		if (this.GetComponent<DropOnArea> ().hasMinionCard != -1 && !isValueModified)
		{
			AssignEffect (element);
		}

		//	Switch off when the field is empty
		if (this.transform.childCount == 0)
			isValueModified = false;
	}

	public void AssignElement (int randomEle)
	{
		switch (randomEle)
		{
			case 1:
				//	None
				this.element = Minion.Element.None;
				this.GetComponent<Image> ().color = origin_color;
				break;
			case 2:
				//	Fire
				this.element = Minion.Element.Fire;
				this.GetComponent<Image> ().color = Color.red;
				break;
			case 3:
				//	Ice
				this.element = Minion.Element.Ice;
				this.GetComponent<Image> ().color = Color.blue;
				break;
			case 4:
				//	Poison
				this.element = Minion.Element.Poison;
				this.GetComponent<Image> ().color = Color.green;
				break;
			case 5:
				//	Electricity
				this.element = Minion.Element.Electricity;
				this.GetComponent<Image> ().color = Color.yellow;
				break;
			default:
				//	None
				this.element = Minion.Element.None;
				this.GetComponent<Image> ().color = origin_color;
				break;
		}
	}

	void AssignEffect (Minion.Element element)
	{
		//	Can be Debuff or Buff, depends on element
		//	Fire -> Ice -> Poison -> Eletricity -> Fire
		//	Same element will cause Buff, Reverse element will cause Debuff
		// 	Either Health or Attack will be deducted by a random value

		bool isBuff = false;

		if (this.transform.childCount != 0)
			target = this.transform.GetChild (0).gameObject;
		else
			return;

		randomValue = Random.Range (1, 4);  //	1-3

		hpOrAtk = Random.Range (0, 2);

		if (element == Minion.Element.None)
		{

		}
		else if (element == Minion.Element.Fire)
		{
			// Same element. Buff
			if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Fire)
				isBuff = true;
			//	Reverse element. Debuff
			else if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Ice)
				isBuff = false;
			else
				return;

			ModifyValue (isBuff);
		}
		else if (element == Minion.Element.Ice)
		{
			// Same element. Buff
			if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Ice)
				isBuff = true;
			//	Reverse element. Debuff
			else if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Poison)
				isBuff = false;
			else
				return;

			ModifyValue (isBuff);
		}
		else if (element == Minion.Element.Poison)
		{
			// Same element. Buff
			if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Poison)
				isBuff = true;
			//	Reverse element. Debuff
			else if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Electricity)
				isBuff = false;
			else
				return;

			ModifyValue (isBuff);
		}
		else if (element == Minion.Element.Electricity)
		{
			// Same element. Buff
			if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Electricity)
				isBuff = true;
			//	Reverse element. Debuff
			else if (target.GetComponent<Minion> ().GetElement () == Minion.Element.Fire)
				isBuff = false;
			else
				return;

			ModifyValue (isBuff);
		}
		else
		{
			Debug.Log ("Should not be here. Something go wrong...");
		}
		isValueModified = true;
	}

	void ModifyValue (bool isBuff)
	{
		if (isBuff)
		{
			if (hpOrAtk == 0)   //	0 = hp
			{
				target.GetComponent<Minion> ().SetHealth (target.GetComponent<Minion> ().GetHealth () + randomValue);
				target.transform.Find ("Text_Health").GetComponent<Text> ().text = target.GetComponent<Minion> ().GetHealth ().ToString ();
			}
			else                //	non 0 = atk
			{
				target.GetComponent<Minion> ().SetAttack (target.GetComponent<Minion> ().GetAttack () + randomValue);
				target.transform.Find ("Text_Attack").GetComponent<Text> ().text = target.GetComponent<Minion> ().GetAttack ().ToString ();
			}
		}
		else
		{
			int debuffOutcome;
			if (hpOrAtk == 0)   //	0 = hp
			{
				//	Debuff cannot make kill to target, the min value of the target would at least be 1.
				if (target.GetComponent<Minion> ().GetHealth () - randomValue <= 0)
					debuffOutcome = 1;
				else
					debuffOutcome = target.GetComponent<Minion> ().GetHealth () - randomValue;

				target.GetComponent<Minion> ().SetHealth (debuffOutcome);
				target.transform.Find ("Text_Health").GetComponent<Text> ().text = target.GetComponent<Minion> ().GetHealth ().ToString ();
			}
			else                //	non 0 = atk
			{
				if (target.GetComponent<Minion> ().GetAttack () - randomValue <= 0)
					debuffOutcome = 1;
				else
					debuffOutcome = target.GetComponent<Minion> ().GetAttack () - randomValue;

				target.GetComponent<Minion> ().SetAttack (debuffOutcome);
				target.transform.Find ("Text_Attack").GetComponent<Text> ().text = target.GetComponent<Minion> ().GetAttack ().ToString ();
			}
		}

	}

	public void setElement (Minion.Element element)
	{
		this.element = element;
	}

	public Minion.Element GetElement ()
	{
		return element;
	}
}
