﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
	[Header ("Reference")]
	[SerializeField] private AudioSource source;

	[Header ("Sound References")]
	[SerializeField] private AudioClip newRound;
	[SerializeField] private AudioClip diceRoll;
	[SerializeField] private AudioClip minionStep;
	[SerializeField] private AudioClip minionDie;
	[SerializeField] private AudioClip buffSpell;
	[SerializeField] private AudioClip debuffSpell;
	[SerializeField] private AudioClip win;
	[SerializeField] private AudioClip lose;

	public void PlaySound (string sound)
	{
		switch (sound)
		{
			case "newRound":
				source.clip = newRound;
				source.Play ();
				break;
			case "diceRoll":
				source.clip = diceRoll;
				source.Play ();
				break;
			case "minionStep":
				source.clip = minionStep;
				source.Play ();
				break;
			case "minionDie":
				source.clip = minionDie;
				source.Play ();
				break;
			case "buffSpell":
				source.clip = buffSpell;
				source.Play ();
				break;
			case "debuffSpell":
				source.clip = debuffSpell;
				source.Play ();
				break;
			case "win":
				source.clip = win;
				source.Play ();
				break;
			case "lose":
				source.clip = lose;
				source.Play ();
				break;
			default:
				Debug.LogError ("Sound track not found");
				break;
		}
	}
}
