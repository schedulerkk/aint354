﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

/* 
 * Referenced from https://www.youtube.com/watch?v=C_Ok4xC_xVU
 */

public class ScreenFader : MonoBehaviour
{
	[SerializeField] private Image img;

	private float backgroundColor_R;
	private float backgroundColor_G;
	private float backgroundColor_B;

	private void Start ()
	{
		backgroundColor_R = img.color.r;
		backgroundColor_G = img.color.g;
		backgroundColor_B = img.color.b;

		// Fade in when every level loads
		StartCoroutine (FadeInScene ());
	}

	private IEnumerator FadeInScene ()
	{
		float t = 1f;

		while (t > 0f)
		{
			t -= Time.deltaTime;
			img.color = new Color (backgroundColor_R, backgroundColor_G, backgroundColor_B, t);
			yield return 0;
		}
	}

	// Wrapper, IEnumerator has to be started by coroutine
	public void FadeToScene (string scene)
	{
		StartCoroutine (FadeTo (scene));
	}

	private IEnumerator FadeTo (string scene)
	{
		float t = 0f;

		while (t < 1f)
		{
			t += Time.deltaTime;
			img.color = new Color (backgroundColor_R, backgroundColor_G, backgroundColor_B, t);
			yield return 0;
		}

		SceneManager.LoadScene (scene);
	}
}
