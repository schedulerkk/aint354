﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonManager : MonoBehaviour
{
	// Reference
	[SerializeField] private Button diceButton;
	[SerializeField] private Sprite [] diceImages;
	[SerializeField] private FieldController fieldController;

	// Variables
	[HideInInspector] public static int dice_number;
	private int currentPage = 0;
	private const float PAGE_TRANSITION_TIME = .5f;

	// ----------------------------- Dice -----------------------------
	public void RollDice ()
	{
		if (!GameFlowController.isRolled && GameFlowController.CheckMinionOnField ())
		{
			GameFlowController.isRolled = true;

			// Actual dice number rolled
			int devDice = FindObjectOfType<DevelopmentMode> ().diceDropDown.value;
			if (devDice == 0 || GameFlowController.playerTurn != 0)
				dice_number = Random.Range (1, 7);
			else
			{
				dice_number = devDice;
				FindObjectOfType<DevelopmentMode> ().diceDropDown.value = 0;
			}
			Debug.Log (((GameFlowController.playerTurn == 0) ? "Player" : "AI") + " - Dice rolled " + dice_number);

			// Play audio effect
			FindObjectOfType<SoundManager> ().PlaySound ("diceRoll");

			// Animation, makes UI look more fancy
			StartCoroutine (AnimateDice ());

			GameFlowController.PlayerCardRightInField ();
			GameFlowController.ToggleDragInPlayerHand (false);

			// Disable the Cards in hand 
			GameObject hands = GameObject.Find ("Panel_Hand_0");
			for (int i = 0; i < hands.transform.childCount; i++)
			{
				hands.transform.GetChild (i).gameObject.GetComponent<Drag> ().enabled = false;
			}
		}
	}

	private IEnumerator AnimateDice ()
	{
		for (int i = 0; i < 10; i++)
		{
			if (i != 0)
				yield return new WaitForSeconds (0.1f);

			diceButton.GetComponent<Image> ().sprite = diceImages [Random.Range (0, 6)];
		}

		diceButton.GetComponent<Image> ().sprite = diceImages [dice_number - 1];

	}

	// ----------------------------- Turns -----------------------------

	public void EndTurn ()
	{
		// Hide the arrows if they are currently displaying to players
		MinionArrowMovement.HideMovementOptions ();

		// Disable End Turn button
		GameObject.Find ("Button_EndTurn").GetComponent<Button> ().interactable = false;
		GameObject.Find ("EndTurnButton").GetComponent<Animator> ().SetBool ("isGlowing", false);

		// Switch player
		GameFlowController.playerTurn = 1 - GameFlowController.playerTurn;
		GameFlowController.currentRound++;

		// Change field effect every 5 rounds
		if (GameFlowController.currentRound % 5 == 0)
		{
			for (int i = 1; i < fieldController.fieldList.Count; i++)
			{
				if (i == 14) continue;
				int randomEle = Random.Range (1, 6);
				fieldController.fieldList [i].GetComponent<FieldEffect> ().AssignElement (randomEle);
			}
		}

		// Reset variables
		GameFlowController.isRolled = false;
		GameFlowController.isMoving = false;
		GameFlowController.isMoved = false;

		FindObjectOfType<GameFlowController> ().StartRound ();

		// Make sure the card is not avaliable for opponent
		GameFlowController.cardRightControll ();
		GameFlowController.PlayerCardRightInField ();

		//Debug.Log ("Turn End! It's player " + GameFlowController.playerTurn + "'s turn now.");
	}

	// ----------------------------- Move Minion by Arrow -----------------------------

	// Wrapper - Buttons need to return void
	public void MoveMinionByArrow (int direction)
	{
		// Hide the arrows
		MinionArrowMovement.HideMovementOptions ();

		// Don't move minions when a minion is moving/has already moved, the player is trying to sneak a bug from you!
		if (GameFlowController.isMoving || GameFlowController.isMoved)
			return;

		// Move the selected minion
		StartCoroutine (MoveMinion (direction));
	}

	private IEnumerator MoveMinion (int direction)
	{
		if (GameFlowController.isMoving)
			yield break;

		GameFlowController.isMoving = true;

		// Toggle drag/glow status
		GameFlowController.PlayerCardRightInField ();

		//Debug.Log ("<color=red>Minion moving direction " + direction + "</color>");
		GameObject movingMinion = MinionArrowMovement.currentSelectedMinion;
		FieldController _fieldController = movingMinion.GetComponent<Minion> ().fieldController;

		// - Field number -

		int originalField = GetFieldNum (movingMinion.transform.parent.name);     // movingMinion's original field

		int wasMiddlePath = (movingMinion.transform.parent.name.Contains ("a") ? 1 : 
			(movingMinion.transform.parent.name.Contains ("b") ? 2 : 0));      // Determines if the previous step was a middle path [0-Halo, 1-PathA, 2-PathB]
		int isMiddlePath = 0;      // Determines if the next step is going to be a middle path [0-Halo, 1-PathA, 2-PathB]
		int nextField = originalField;     // movingMinion's proceeding field
		bool isEncountered = false;
		string tempOrigin = movingMinion.transform.parent.name;     // Stores temp position in every dice count iteration
		string currentField = movingMinion.transform.parent.name;
		int _direction = direction;

		for (int i = dice_number; i > 0; i--)     // i = Dice remaining steps
		{
			//Debug.Log ("<color=red>Dice iteration: " + i + "</color>");

			bool exitMiddlePath = false;        // Indicates whether the minion has exit a middle path in this iteration

			currentField = movingMinion.transform.parent.name;

			if (_direction == 1)     // Forward
				nextField++;
			else if (_direction == -1)      // Backward
				nextField--;
			else if (_direction == 0)   // Start going middle path from halo
			{
				if (currentField == "4")
				{
					isMiddlePath = 1;
					nextField = 0;
					_direction = 1;
				}
				if (currentField == "10")
				{
					isMiddlePath = 1;
					nextField = 4;
					_direction = -1;
				}
				if (currentField == "18")
				{
					isMiddlePath = 2;
					nextField = 0;
					_direction = 1;
				}
				if (currentField == "24")
				{
					isMiddlePath = 2;
					nextField = 4;
					_direction = -1;
				}
			}

			// - Check if next field is out of bounds -
			if (currentField.Contains ("a"))                // Currently at middle path A
			{
				isMiddlePath = 1;

				// Exit Middle Path
				if (_direction == 1 && nextField > _fieldController.middleFieldsA.Length - 1)
				{
					isMiddlePath = 0;
					nextField = 10;
					exitMiddlePath = true;
				}
				else if (_direction == -1 && nextField < 0)
				{
					isMiddlePath = 0;
					nextField = 4;
					exitMiddlePath = true;
				}
			}
			else if (currentField.Contains ("b"))       // Currently at middle path B
			{
				isMiddlePath = 2;

				// Exit Middle Path
				if (_direction == 1 && nextField > _fieldController.middleFieldsA.Length - 1)
				{
					isMiddlePath = 0;
					nextField = 24;
					exitMiddlePath = true;
				}
				else if (_direction == -1 && nextField < 0)
				{
					isMiddlePath = 0;
					nextField = 18;
					exitMiddlePath = true;
				}
			}
			else
			{
				if (nextField > _fieldController.fieldList.Count - 1)
					nextField = 0;
				else if (nextField < 0)
					nextField = _fieldController.fieldList.Count - 1;
			}

			// - Proceed if there is no minion blocking in front -
			bool proceed = false;

			//print ("<color=red>nextField = " + nextField + "</color>");
			if (isMiddlePath == 0)
				proceed = _fieldController.fieldList [nextField].GetComponent<DropOnArea> ().hasMinionCard == -1;
			else if (isMiddlePath == 1)
				proceed = _fieldController.middleFieldsA [nextField].GetComponent<DropOnArea> ().hasMinionCard == -1;
			else if (isMiddlePath == 2)
				proceed = _fieldController.middleFieldsB [nextField].GetComponent<DropOnArea> ().hasMinionCard == -1;

			if (proceed)
			{
				tempOrigin = (isMiddlePath == 1 ? "a" : isMiddlePath == 2 ? "b" : "") + nextField;

				//print ("<color=red>set parent " + tempOrigin + "</color>");
				movingMinion.transform.SetParent (GameObject.Find (tempOrigin).transform);
				movingMinion.GetComponent<Minion> ().parentReturnto = movingMinion.transform.parent;

				// Play audio effect
				FindObjectOfType<SoundManager> ().PlaySound ("minionStep");

				// Give some time between each step to make the minion looks like walking towards the target field
				if (i > 1)
					yield return new WaitForSeconds (0.5f);
			}
			else        // There is a minion in front, stop right here
			{
				Debug.Log ("Minion encountered @ Field " + nextField + " - Breaking movement loop...");
				isEncountered = true;
				break;
			}

			if (exitMiddlePath)
			{
				Debug.Log ("Minion exit middle path - Breaking movement loop...");
				break;
			}

			// - Check if any player has reached his opponent's base & End the game -
			if (movingMinion.GetComponent<Minion> ().cardRef == 0 && nextField == 14)
			{
				FindObjectOfType<GameFlowController> ().WinGame (0);
				yield break;
			}
			else if (movingMinion.GetComponent<Minion> ().cardRef == 1 && nextField == 0)
			{
				FindObjectOfType<GameFlowController> ().WinGame (1);
				yield break;
			}
		}

		// Stay back on the previous field
		if (isEncountered)
		{
			if (tempOrigin.Contains ("a"))
				isMiddlePath = 1;
			else if (tempOrigin.Contains ("b"))
				isMiddlePath = 2;
			else
				isMiddlePath = 0;

			nextField = GetFieldNum (tempOrigin);
		}

		// - Update field status after the movement is complete -
		// Original field
		if (wasMiddlePath == 0)		// Halo
			_fieldController.fieldList [originalField].GetComponent<DropOnArea> ().hasMinionCard = -1;
		else if (wasMiddlePath == 1)		// Path A
			_fieldController.middleFieldsA [originalField].GetComponent<DropOnArea> ().hasMinionCard = -1;
		else        // Path B
			_fieldController.middleFieldsB [originalField].GetComponent<DropOnArea> ().hasMinionCard = -1;

		// Current field
		if (isMiddlePath == 0)          // Halo
			_fieldController.fieldList [nextField].GetComponent<DropOnArea> ().hasMinionCard = (movingMinion != null && movingMinion.GetComponent<Card> ().GetHealth () > 0 ? GameFlowController.playerTurn : -1);
		else if (isMiddlePath == 1)   // Path A
			_fieldController.middleFieldsA [nextField].GetComponent<DropOnArea> ().hasMinionCard = (movingMinion != null && movingMinion.GetComponent<Card> ().GetHealth () > 0 ? GameFlowController.playerTurn : -1);
		else     // Path B
			_fieldController.middleFieldsB [nextField].GetComponent<DropOnArea> ().hasMinionCard = (movingMinion != null && movingMinion.GetComponent<Card> ().GetHealth () > 0 ? GameFlowController.playerTurn : -1);

		//Debug.Log ("Set field (" + nextField + ") to " + (movingMinion != null && movingMinion.GetComponent<Card> ().GetHealth () > 0 ? GameFlowController.playerTurn : -1));

		// - Try to attack nearby enemies -
		movingMinion.GetComponent<Minion> ().Attack (_direction == 1);
		//print ("<color=blue>Attack called on direction(" + direction + ") @ field (" + movingMinion.transform.parent.name + ")</color>");

		// The whole movement process has completed
		// isMoved variable will be updated in Attack method of Minion to continue actions on other scripts
	}

	private int GetFieldNum (string fieldName)
	{
		string fieldNum = string.Empty;
		for (int i = 0; i < fieldName.Length; i++)
		{
			if (char.IsDigit (fieldName [i]))
			{
				fieldNum += fieldName [i];
			}
		}

		return int.Parse (fieldNum);
	}

	// ----------------------------- Main Menu -----------------------------

	public void StartGame ()
	{
		FindObjectOfType<ScreenFader> ().FadeToScene ("Game");
	}

	public void Tutorial ()
	{
		FindObjectOfType<ScreenFader> ().FadeToScene ("Tutorial");
	}

	public void QuitGame ()
	{
		Application.Quit ();
	}

	// ----------------------------- Tutorial -----------------------------

	public void NextPage ()
	{
		if (currentPage >= GameObject.Find ("Pages").transform.childCount - 1)
			return;

		currentPage++;
		StartCoroutine (LerpObject ());
	}

	public void PreviousPage ()
	{
		if (currentPage <= 0)
			return;

		currentPage--;
		StartCoroutine (LerpObject ());
	}

	// Code from https://answers.unity.com/questions/1240045/how-to-smoothly-move-object-with-recttransform.html
	IEnumerator LerpObject ()
	{
		float currentTime = 0;
		float normalizedValue;
		RectTransform rectTransform = GameObject.Find ("Pages").GetComponent<RectTransform> ();
		Vector3 startPosition = rectTransform.anchoredPosition;
		Vector3 endPosition = new Vector3 (currentPage * -1920f, 0, 0);

		while (currentTime <= PAGE_TRANSITION_TIME)
		{
			currentTime += Time.deltaTime;
			normalizedValue = currentTime / PAGE_TRANSITION_TIME;   // Normalize time

			rectTransform.anchoredPosition = Vector3.Lerp (startPosition, endPosition, normalizedValue);
			yield return null;
		}
	}

	// ----------------------------- Dev Mode -----------------------------

	public void WinGame (int player)
	{
		FindObjectOfType<DevelopmentMode> ().ToggleDevMode (false);
		FindObjectOfType<GameFlowController> ().WinGame (player);
	}

	// ----------------------------- Game Over -----------------------------

	public void PlayAgain ()
	{
		FindObjectOfType<ScreenFader> ().FadeToScene ("Game");
	}

	public void MainMenu ()
	{
		FindObjectOfType<ScreenFader> ().FadeToScene ("MainMenu");
	}
}
