﻿using UnityEngine;
using UnityEngine.UI;

public class DevelopmentMode : MonoBehaviour
{
	public enum MinionMoveMode
	{
		DragNDrop,
		Arrow
	}

	public static MinionMoveMode minionMoveMode = MinionMoveMode.Arrow;     // Initial value set

	[Header ("References")]
	[SerializeField] private GameObject devPanel;
	[SerializeField] private Dropdown minionMovementDropDown;
	public Dropdown diceDropDown;

	private bool devMode;

	void Start ()
	{
		devMode = false;
		devPanel.SetActive (false);

		// Assign initial value to drop down list
		minionMovementDropDown.value = (int)minionMoveMode;

		minionMovementDropDown.onValueChanged.AddListener (delegate
		{
			DropdownValueChanged (minionMovementDropDown);
		});
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.D))
		{
			ToggleDevMode (!devMode);
		}
	}

	// Called when value changed, toggle gameplay immediately
	void DropdownValueChanged (Dropdown change)
	{
		// Assign new movement mode
		minionMoveMode = (MinionMoveMode)change.value;

		//Debug.Log ("Movement mode switched to " + (MinionMoveMode)change.value);

		// Apply changes to all existing cards
		switch (change.value)
		{
			case 0:         // Drag and Drop
				GameFlowController.cardRightControll ();
				break;
			case 1:         // Switched to Arrow mode, disable all card 'Drag' component
				FieldController fieldController = GameObject.Find ("FieldController").GetComponent<FieldController> ();

				for (int i = 0; i < fieldController.fieldList.Count; i++)
				{
					if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
					{
						fieldController.fieldList [i].transform.Find ("Card(Clone)").gameObject.GetComponent<Drag> ().enabled = false;
					}
				}
				break;
		}
	}

	public void ToggleDevMode (bool devOn)
	{
		if (GameFlowController.isGameOver)
		{
			return;
		}

		devMode = devOn;
		devPanel.SetActive (devMode);
	}
}
