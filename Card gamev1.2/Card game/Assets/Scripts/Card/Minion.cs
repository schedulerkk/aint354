﻿using UnityEngine;
using UnityEngine.UI;

public class Minion : Card
{
	// - Elements -
	public enum Element
	{
		None,
		Fire,
		Ice,
		Poison,
		Electricity
	};

	// --- Minion attributes ---
	private Element element;
	// [PH]Active/Passive skills

	[Header ("Reference")]
	[SerializeField] private GameObject minionHitImage;

	// --- Variables ---
	[HideInInspector] public GameObject targetedfield_1 = null;
	[HideInInspector] public GameObject targetedfield_2 = null;

	[HideInInspector] public bool inField;      // Determines whether this card is already in field
	[HideInInspector] public GameObject temp_tf;
	[HideInInspector] public GameObject temp_tf1;
	[HideInInspector] public GameObject temp_tf2;
	[HideInInspector] public int location;
	[HideInInspector] public int stepForward;
	[HideInInspector] public int stepBackward;
	[HideInInspector] public bool isArrived = false;

	// Use this for initialization
	protected override void Start ()
	{
		base.Start ();

		// Get the field list
		fieldController = GameObject.Find ("FieldController").GetComponent<FieldController> ();

		inField = false;
	}

	// -- Element --
	public Element GetElement ()
	{
		return element;
	}
	public void SetElement (Element element)
	{
		this.element = element;
	}

	// Reset Method
	public override void ResetCard ()
	{
		base.ResetCard ();

		element = Element.None;
	}

	// Attack an enemy if there is one in the specified direction
	public void Attack (bool directionFront)
	{
		// Get current field number, Bug Fix: field b1 will now get 1 instead of 0
		string _fieldNum = transform.parent.name;
		string fieldNum = string.Empty;
		for (int i = 0; i < _fieldNum.Length; i++)
		{
			if (char.IsDigit (_fieldNum [i]))
			{
				fieldNum += _fieldNum [i];
			}
		}
		int currentField = int.Parse (fieldNum);
		//print ("<color=blue>parent name = " + transform.parent.name + " fieldNum = " + currentField + "</color>");

		GameObject forwardField = null, backwardField = null;
		Minion enemy;
		int relativeEnemy;

		// Figure out the current path the minion is in [0-halo, 1-PathA, 2-PathB]
		int currentPath;
		if (parentReturnto.name.Contains ("a"))
			currentPath = 1;
		else if (parentReturnto.name.Contains ("b"))
			currentPath = 2;
		else
			currentPath = 0;

		GameObject fc = GameObject.Find ("FieldController");
		FieldController fieldController = FindObjectOfType<FieldController> ();

		relativeEnemy = 1 - cardRef;

		Debug.Log ("relativeEnemy : " + relativeEnemy);

		// -- Attack according to direction --
		if (directionFront)
		{
			// - Assign target fields -
			if (currentPath == 0)       // Halo path
			{
				if (currentField == fieldController.fieldList.Count - 1)
					forwardField = fieldController.fieldList [0];
				else
					forwardField = fieldController.fieldList [currentField + 1];
			}
			else if (currentPath == 1)      // Middle Path A
			{
				if (currentField == fieldController.middleFieldsA.Length - 1)
					forwardField = fieldController.fieldList [10];
				else
					forwardField = fieldController.middleFieldsA [currentField + 1];
			}
			else if (currentPath == 2)      // Middle Path B
			{
				if (currentField == fieldController.middleFieldsB.Length - 1)
					forwardField = fieldController.fieldList [24];
				else
					forwardField = fieldController.middleFieldsB [currentField + 1];
			}

			// Attack, if target is an enemy
			if (forwardField.GetComponent<DropOnArea> ().hasMinionCard == relativeEnemy)
			{
				enemy = forwardField.transform.Find ("MinionCard(Clone)").GetComponent<Minion> ();

				TakeDamage (enemy.GetAttack ());
				enemy.TakeDamage (GetAttack ());
			}
		}
		else
		{
			// - Assign target fields -
			if (currentPath == 0)       // Halo path
			{
				if (currentField == 0)
					backwardField = fieldController.fieldList [fieldController.fieldList.Count - 1];
				else
					backwardField = fieldController.fieldList [currentField - 1];
			}
			else if (currentPath == 1)      // Middle Path A
			{
				if (currentField == 0)
					backwardField = fieldController.fieldList [4];
				else
					backwardField = fieldController.middleFieldsA [currentField - 1];
			}
			else if (currentPath == 2)      // Middle Path B
			{
				if (currentField == 0)
					backwardField = fieldController.fieldList [18];
				else
					backwardField = fieldController.middleFieldsB [currentField - 1];
			}

			// Attack, if target is an enemy
			if (backwardField.GetComponent<DropOnArea> ().hasMinionCard == relativeEnemy)
			{
				enemy = backwardField.transform.Find ("MinionCard(Clone)").GetComponent<Minion> ();

				TakeDamage (enemy.GetAttack ());
				enemy.TakeDamage (GetAttack ());
			}
		}
		//Debug.Log (forwardField.GetComponent<DropOnArea> ().hasMinionCard);
		//Debug.Log (backwardfield.GetComponent<DropOnArea> ().hasMinionCard);

		// Movement & Attack done, update isMoved variable to continue actions on other scripts
		GameFlowController.isMoved = true;
	}

	// Reducing health points of this minion (enemy attacks, spells, etc)
	public void TakeDamage (int damage)
	{
		SetHealth (GetHealth () - damage);

		// - Display received hit damage to player -
		GameObject minionHitDamageUI = Instantiate (minionHitImage, gameObject.transform);
		minionHitDamageUI.transform.GetChild (0).GetComponent<Text> ().text = "-" + damage.ToString ();
		Destroy (minionHitDamageUI, 0.5f);

		// - Update current health into UI -
		transform.Find ("Text_Health").GetComponent<Text> ().text = GetHealth ().ToString ();

		// - Destroy minion if health reaches 0 -
		if (GetHealth () <= 0)
		{
			Destroy (gameObject, 0.6f);
			this.transform.parent.GetComponent<DropOnArea> ().hasMinionCard = -1;
			//Debug.Log(this.transform.parent.name);

			// Play audio effect
			FindObjectOfType<SoundManager> ().PlaySound ("minionDie");
		}
	}
}
