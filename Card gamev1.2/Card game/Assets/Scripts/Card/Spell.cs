﻿using UnityEngine;

public class Spell : Card
{
	public enum SpellType
	{
		Buff,
		Debuff
	}

	// --- Spell attributes ---
	private SpellType spellType;		// Indicates the type of the spell

	// Use this for initialization
	protected override void Start ()
	{
		base.Start ();
	}

	// -- Spell Type --
	public SpellType GetSpellType ()
	{
		return spellType;
	}
	public void SetSpellType (SpellType spellType)
	{
		this.spellType = spellType;
	}
}
