﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ProceduralCardGenerator : MonoBehaviour, ISerializationCallbackReceiver
{
	private enum GeneratingLevel
	{
		Normal,
		Less,
		Least
	};

	// --- Customizable Inspector Variables ---
	[Header ("- Game Control -")]
	[Tooltip ("Defines how the cards will benefit the player")]
	[SerializeField] private GeneratingLevel playerGeneratingLevel = GeneratingLevel.Normal;
	[Tooltip ("Defines how the cards will benefit the AI")]
	[SerializeField] private GeneratingLevel AIGeneratingLevel = GeneratingLevel.Less;
	[Tooltip ("Defines whether the system will somehow benefit the player when he/she is losing the game")]
	[HideInInspector] public bool losingBenefit = true;
	[HideInInspector] public float losingMultiplyBase;
	//[Tooltip ("Defines whether the system will add/reduce the cost of the card if the player is lucky enough")]
	//[SerializeField] private bool luckyCost = true;
	[Tooltip ("Defines the probability of a player drawing a minion instead of a spell card, spell = 1-minion%")]
	[Range (0f, 1f)] public float minionCardPercentage;       // Referenced from CardDrawer.cs

	//[Header ("- Cost -")]
	//[SerializeField] [Range (1, 10)] private int costLowerLimit;
	//[SerializeField] [Range (1, 10)] private int costUpperLimit;

	[Header ("- Minion -")]
	//[Tooltip ("On a scale of 0f~1f, how will the minion attack attribute affect the cost?")]
	//[SerializeField] [Range (0f, 1f)] private float minionAttackCostRatio;
	[Range (1, 10)] public int minionAttackLowerLimit;		// Referenced from Drag.cs (Magic cards)
	[Range (1, 10)] public int minionAttackUpperLimit;     // Referenced from Drag.cs (Magic cards)
	[SerializeField] private AnimationCurve minionAttackCurve;

	//[Tooltip ("On a scale of 0f~1f, how will the minion health attribute affect the cost?")]
	//[SerializeField] [Range (0f, 1f)] private float minionHealthCostRatio;
	[Range (1, 10)] public int minionHealthLowerLimit;      // Referenced from Drag.cs (Magic cards)
	[Range (1, 10)] public int minionHealthUpperLimit;      // Referenced from Drag.cs (Magic cards)
	[SerializeField] private AnimationCurve minionHealthCurve;

	[Header ("- Spell -")]
	[Tooltip ("Defines the probability of a player drawing a buff spell instead of a debuff, debuff = 1-buff%")]
	[SerializeField] [Range (0f, 1f)] private float buffSpellPercentage;
	//[Tooltip ("On a scale of 0f~1f, how will the spell attack attribute affect the cost?")]
	//[SerializeField] [Range (0f, 1f)] private float spellAttackCostRatio;
	//[Tooltip ("On a scale of 0f~1f, how will the spell health attribute affect the cost?")]
	//[SerializeField] [Range (0f, 1f)] private float spellHealthCostRatio;

	[Header ("Buff Spell")]
	[SerializeField] [Range (0, 10)] private int buffSpellAttackLowerLimit;
	[SerializeField] [Range (0, 10)] private int buffSpellAttackUpperLimit;
	[SerializeField] private AnimationCurve buffSpellAttackCurve;
	[SerializeField] [Range (0, 10)] private int buffSpellHealthLowerLimit;
	[SerializeField] [Range (0, 10)] private int buffSpellHealthUpperLimit;
	[SerializeField] private AnimationCurve buffSpellHealthCurve;

	[Header ("Debuff Spell")]
	[SerializeField] [Range (0, 10)] private int debuffSpellAttackLowerLimit;
	[SerializeField] [Range (0, 10)] private int debuffSpellAttackUpperLimit;
	[SerializeField] private AnimationCurve debuffSpellAttackCurve;
	[SerializeField] [Range (0, 10)] private int debuffSpellHealthLowerLimit;
	[SerializeField] [Range (0, 10)] private int debuffSpellHealthUpperLimit;
	[SerializeField] private AnimationCurve debuffSpellHealthCurve;

	[Header ("- Object References -")]
	[SerializeField] private Minion tempMinion;     // Stores the attributes for a new card temporarily
	[SerializeField] private Spell tempSpell;     // Stores the attributes for a new card temporarily
	[SerializeField] private FieldController fc;
	[SerializeField] private Sprite[] minionArtwork;
	[SerializeField] private Sprite[] buffSpellArtwork;
	[SerializeField] private Sprite[] debuffSpellArtwork;

	private float GetLevelPercentage (GeneratingLevel value)
	{
		switch (value)
		{
			case GeneratingLevel.Normal:
				return 1f;
			case GeneratingLevel.Less:
				return 0.85f;
			case GeneratingLevel.Least:
				return 0.7f;
			default:
				Debug.LogError ("Undefined generating value index [ " + playerGeneratingLevel + " ] in GetLevelPercentage ()");
				return 0f;
		}
	}

	private float GetLosingBenefit (int player)
	{
		if (losingBenefit)
		{
			int minionOnField = 0;      // Enemy minion count

			for (int i = 0; i < fc.fieldList.Count; i++)
			{
				if (fc.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard != -1 &&
					fc.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard != player)       // Enemy minion here
				{
					minionOnField++;
				}
			}

			//Debug.Log ("Player " + player + " >> " + minionOnField + " enemy minion" + ((minionOnField > 1) ? "s" : "") + " on field");

			return minionOnField * losingMultiplyBase;
		}
		return 0;
	}

	// - Core of Game Flow / Dynamic Game Balancing -
	// Generate a new card according to the game progress (generatingLevel, losingBenefit, etc.) + probability :-)
	public Minion GenerateMinion (int player)
	{
		float randomCurveX = Random.value;     // Value used to determine which curve value we will take (Probability)
		//Debug.Log ("randomCurveX = " + randomCurveX);

		// Attribute values by randomizing from curve
		float attackValue, healthValue;

		// Actual card attributes
		int cost;
		int attack, health;
		Minion.Element element = Minion.Element.None;

		// Generating values level determination
		float generatingLevel = (player == 0) ? GetLevelPercentage (playerGeneratingLevel) : GetLevelPercentage (AIGeneratingLevel);

		float lbValue = GetLosingBenefit (player);

		// - Attack Damage - (Random using curve)
		attackValue = minionAttackCurve.Evaluate (randomCurveX);
		attack = Mathf.FloorToInt (((attackValue * (minionAttackUpperLimit - minionAttackLowerLimit + 1)) + minionAttackLowerLimit) * generatingLevel + lbValue);
		attack = Mathf.Clamp (attack, minionAttackLowerLimit, minionAttackUpperLimit);

		// - Health Point - (Random using curve)
		healthValue = minionHealthCurve.Evaluate (randomCurveX);
		health = Mathf.FloorToInt (((healthValue * (minionHealthUpperLimit - minionHealthLowerLimit + 1)) + minionHealthLowerLimit) * generatingLevel + lbValue);
		health = Mathf.Clamp (health, minionHealthLowerLimit, minionHealthUpperLimit);

		// - Element - (Random using floats)
		float elementValue;     // Used as a criteria of calculating the card's cost (0f ~ 1f), can be freely adjusted according to the worth of element
		float elementProbability = Random.Range (0f, 1f);
		if (elementProbability < 0.28f)            // Fire - 28%
		{
			element = Minion.Element.Fire;
			elementValue = 0.45f;
		}
		else if (elementProbability < 0.52f)       // Ice - 24%
		{
			element = Minion.Element.Ice;
			elementValue = 0.5f;
		}
		else if (elementProbability < 0.78f)       // Poison - 26%
		{
			element = Minion.Element.Poison;
			elementValue = 0.7f;
		}
		else        // Electricity - 22%
		{
			element = Minion.Element.Electricity;
			elementValue = 0.8f;
		}

		// - Calculate how much this card should cost according to the above attributes (Game Balancing) -
		//float cardValue = ((attackValue * minionAttackCostRatio) + (healthValue * minionHealthCostRatio) + (elementValue)) / 3;
		//cost = Mathf.FloorToInt ((cardValue * (costUpperLimit - costLowerLimit + 1)) + costLowerLimit);

		// Add some lucky elements to the system, lets see if the player is lucky or not >w< (Random by switching integers)
		/*if (luckyCost)
		{
			int costChangeChance = Random.Range (0, 5);
			switch (costChangeChance)
			{
				case 0:
				case 1:
				case 2:
					// Nothing changes (60%)
					Debug.Log ("<color=brown>Fair enough, current card cost is " + cost + "</color>");
					break;
				case 3:
					// Lucky Zone!! (20%)
					int changeValue = Random.Range (1, 3);
					cost -= changeValue;
					Debug.Log ("<color=brown>Lucky! Cost has decreased by " + changeValue + " , current card cost is " + cost + "</color>");
					break;
				case 4:
					// The player got unlucky...hopefully he/she doesn't know anything =.=' (20%)
					changeValue = Random.Range (1, 3);
					cost += changeValue;
					Debug.Log ("<color=brown>Unlucky! Cost has increased by " + changeValue + " , current card cost is " + cost + "</color>");
					break;
			}

			// Make sure the cost is still covered of the limit
			cost = Mathf.Clamp (cost, costLowerLimit, costUpperLimit);
		}*/

		// Create a new card object and apply the above generated attributes into it
		/*Card card = new Card ();		// Using new keyword to instantiate card object is not allowed
			you can imagine MonoBehaviour needs to be attach to an object in scene
			use an empty object with a card script to store attributes temporarily*/
		tempMinion.ResetCard ();
		tempMinion.SetName ("---");
		tempMinion.SetAttack (attack);
		tempMinion.SetHealth (health);
		tempMinion.SetArtwork (minionArtwork [Random.Range (0, minionArtwork.Length)]);
		//tempMinion.SetCost (cost);
		tempMinion.SetCardType (Card.CardType.Minion);

		tempMinion.GetComponent<Minion> ().SetElement (element);

		Debug.Log ("<color=blue>New minion generated - Name (" + tempMinion.GetName () +
			") || Attack (" + tempMinion.GetAttack () + ") || Health ("
			+ tempMinion.GetHealth () + ") || Element (" + tempMinion.GetComponent<Minion> ().GetElement () + ")" + " </color>");

		return tempMinion;
	}

	public Spell GenerateSpell (int player)
	{
		float randomCurveX = Random.value;     // Value used to determine which curve value we will take (Probability)
		//Debug.Log ("randomCurveX = " + randomCurveX);

		// Attribute values by randomizing from curve
		float attackValue, healthValue;

		// Actual card attributes
		int cost;
		int attack, health;

		// Generating values level determination
		float generatingLevel = (player == 0) ? GetLevelPercentage (playerGeneratingLevel) : GetLevelPercentage (AIGeneratingLevel);

		float lbValue = GetLosingBenefit (player);

		// - Determines whether this card is a buff or a debuff -
		float buffSpell = Random.value;
		bool isBuff = (buffSpell > 1 - buffSpellPercentage);

		if (isBuff)
		{
			// Random values using curve
			attackValue = buffSpellAttackCurve.Evaluate (randomCurveX);
			healthValue = buffSpellHealthCurve.Evaluate (randomCurveX);

			attack = Mathf.FloorToInt (((attackValue * (buffSpellAttackUpperLimit - buffSpellAttackLowerLimit + 1)) + buffSpellAttackLowerLimit) * generatingLevel + lbValue);
			attack = Mathf.Clamp (attack, buffSpellAttackLowerLimit, buffSpellAttackUpperLimit);
			health = Mathf.FloorToInt (((healthValue * (buffSpellHealthUpperLimit - buffSpellHealthLowerLimit + 1)) + buffSpellHealthLowerLimit) * generatingLevel + lbValue);
			health = Mathf.Clamp (health, buffSpellHealthLowerLimit, buffSpellHealthUpperLimit);
		}
		else
		{
			// Random values using curve
			attackValue = debuffSpellAttackCurve.Evaluate (randomCurveX);
			healthValue = debuffSpellHealthCurve.Evaluate (randomCurveX);

			attack = Mathf.FloorToInt (((attackValue * (debuffSpellAttackUpperLimit - debuffSpellAttackLowerLimit + 1)) + debuffSpellAttackLowerLimit) * generatingLevel + lbValue);
			attack = Mathf.Clamp (attack, debuffSpellAttackLowerLimit, debuffSpellAttackUpperLimit);
			health = Mathf.FloorToInt (((healthValue * (debuffSpellHealthUpperLimit - debuffSpellHealthLowerLimit + 1)) + debuffSpellHealthLowerLimit) * generatingLevel + lbValue);
			health = Mathf.Clamp (health, debuffSpellHealthLowerLimit, debuffSpellHealthUpperLimit);
		}

		// - Calculate how much this card should cost according to the above attributes (Game Balancing) -
		//float cardValue = ((attackValue * spellAttackCostRatio) + (healthValue * spellHealthCostRatio)) / 2;
		//cost = Mathf.FloorToInt ((cardValue * (costUpperLimit - costLowerLimit + 1)) + costLowerLimit);

		tempSpell.ResetCard ();
		tempSpell.SetName ("---");
		tempSpell.SetAttack (isBuff ? attack : -attack);
		tempSpell.SetHealth (isBuff ? health : -health);
		tempSpell.SetArtwork (isBuff ? buffSpellArtwork [Random.Range (0, buffSpellArtwork.Length)] :
			debuffSpellArtwork [Random.Range (0, debuffSpellArtwork.Length)]);
		//tempSpell.SetCost (cost);
		tempSpell.SetCardType (Card.CardType.Spell);

		tempSpell.GetComponent<Spell> ().SetSpellType (isBuff ? Spell.SpellType.Buff : Spell.SpellType.Debuff);

		Debug.Log ("<color=blue>New spell generated - Name (" + tempSpell.GetName () + " || Type (" + 
			tempSpell.GetSpellType () + ")" + ") || Attack (" + tempSpell.GetAttack () + 
			") || Health (" + tempSpell.GetHealth () + ") </color>");

		return tempSpell;
	}

	// Validate variables after user customization, works well whenever value changes
	public void OnAfterDeserialize ()
	{
		// -- Negative values handled by range already --

		// -- Check if the lower limit is higher than upper limit --
		int lowerLimit, upperLimit;     // Stores original variable values for debug messages

		// Cost
		/*if (costLowerLimit > costUpperLimit)
		{
			lowerLimit = costLowerLimit;
			upperLimit = costUpperLimit;
			costUpperLimit = costLowerLimit;
			//Debug.LogWarning ("Customization Error - [costLowerLimit ( " + lowerLimit + " ) > costUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current costLowerLimit ( " + costLowerLimit + " ) costUpperLimit ( " + costUpperLimit + " )");
			return;
		}*/

		// Minion Attack Damage
		if (minionAttackLowerLimit > minionAttackUpperLimit)
		{
			lowerLimit = minionAttackLowerLimit;
			upperLimit = minionAttackUpperLimit;
			minionAttackUpperLimit = minionAttackLowerLimit;
			//Debug.LogWarning ("Customization Error - [attackLowerLimit ( " + lowerLimit + " ) > attackUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current attackLowerLimit ( " + minionAttackLowerLimit + " ) attackUpperLimit ( " + minionAttackUpperLimit + " )");
			return;
		}

		// Minion Health Point
		if (minionHealthLowerLimit > minionHealthUpperLimit)
		{
			lowerLimit = minionHealthLowerLimit;
			upperLimit = minionHealthUpperLimit;
			minionHealthUpperLimit = minionHealthLowerLimit;
			//Debug.LogWarning ("Customization Error - [healthLowerLimit ( " + lowerLimit + " ) > healthUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current healthLowerLimit ( " + minionHealthLowerLimit + " ) healthUpperLimit ( " + minionHealthUpperLimit + " )");
			return;
		}

		// Buff Spell Attack
		if (buffSpellAttackLowerLimit > buffSpellAttackUpperLimit)
		{
			lowerLimit = buffSpellAttackLowerLimit;
			upperLimit = buffSpellAttackUpperLimit;
			buffSpellAttackUpperLimit = buffSpellAttackLowerLimit;
			//Debug.LogWarning ("Customization Error - [buffSpellAttackLowerLimit ( " + lowerLimit + " ) > buffSpellAttackUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current buffSpellAttackLowerLimit ( " + buffSpellAttackLowerLimit + " ) buffSpellAttackUpperLimit ( " + buffSpellAttackUpperLimit + " )");
			return;
		}

		// Buff Spell Health
		if (buffSpellHealthLowerLimit > buffSpellHealthUpperLimit)
		{
			lowerLimit = buffSpellHealthLowerLimit;
			upperLimit = buffSpellHealthUpperLimit;
			buffSpellHealthUpperLimit = buffSpellHealthLowerLimit;
			//Debug.LogWarning ("Customization Error - [buffSpellHealthLowerLimit ( " + lowerLimit + " ) > buffSpellHealthUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current buffSpellHealthLowerLimit ( " + buffSpellHealthLowerLimit + " ) buffSpellHealthUpperLimit ( " + buffSpellHealthUpperLimit + " )");
			return;
		}

		// Debuff Spell Attack
		if (debuffSpellAttackLowerLimit > debuffSpellAttackUpperLimit)
		{
			lowerLimit = debuffSpellAttackLowerLimit;
			upperLimit = debuffSpellAttackUpperLimit;
			debuffSpellAttackUpperLimit = debuffSpellAttackLowerLimit;
			//Debug.LogWarning ("Customization Error - [debuffSpellAttackLowerLimit ( " + lowerLimit + " ) > debuffSpellAttackUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current debuffSpellAttackLowerLimit ( " + debuffSpellAttackLowerLimit + " ) debuffSpellAttackUpperLimit ( " + debuffSpellAttackUpperLimit + " )");
			return;
		}

		// Debuff Spell Health
		if (debuffSpellHealthLowerLimit > debuffSpellHealthUpperLimit)
		{
			lowerLimit = debuffSpellHealthLowerLimit;
			upperLimit = debuffSpellHealthUpperLimit;
			debuffSpellHealthUpperLimit = debuffSpellHealthLowerLimit;
			//Debug.LogWarning ("Customization Error - [debuffSpellHealthLowerLimit ( " + lowerLimit + " ) > debuffSpellHealthUpperLimit ( " + upperLimit + " ) ] - Fixed in runtime - Current debuffSpellHealthLowerLimit ( " + debuffSpellHealthLowerLimit + " ) debuffSpellHealthUpperLimit ( " + debuffSpellHealthUpperLimit + " )");
			return;
		}
	}

	public void OnBeforeSerialize () { }
}

// Custom Editor
// Reference: https://answers.unity.com/questions/1284988/custom-inspector-2.html
#if UNITY_EDITOR
[CustomEditor (typeof (ProceduralCardGenerator))]
public class RandomScript_Editor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();        // For other non-HideInInspector fields

		ProceduralCardGenerator script = (ProceduralCardGenerator)target;

		EditorGUILayout.Space ();
		EditorGUILayout.LabelField ("Losing Benefit", EditorStyles.boldLabel);
		script.losingBenefit = EditorGUILayout.Toggle ("Losing Benefit", script.losingBenefit);
		if (script.losingBenefit)
		{
			script.losingMultiplyBase = EditorGUILayout.FloatField ("Multiply Base", 0.5f);
		}
	}
}
#endif