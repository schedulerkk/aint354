﻿using UnityEngine;

public class Card : MonoBehaviour
{
	public enum CardType
	{
		Minion,
		Spell
	}
	
	// --- Card attributes ---
	private new string name;
	private int attack;
	private int health;
	private Sprite artwork;
	//private int cost;
	[HideInInspector] public int cardRef = 0;     // The player this card is belong to
	private CardType cardType;

	// -----------------------------------------------

	[HideInInspector] public Transform parentReturnto = null;     // The field this card is going to
	[HideInInspector] public Transform cardPlaceParent = null;  // The placeholder that stores the place the card is going to
	[HideInInspector] public Transform previousParent = null;       // The original parent of this card
	[HideInInspector] public Transform owner = null;                    // Player/AI 's panel
	[HideInInspector] public FieldController fieldController;

	// -----------------------------------------------

	private int chance;

	protected virtual void Start ()
	{
		
	}

	// --- Get/Set Methods ---
	// -- Name --
	public string GetName ()
	{
		return name;
	}
	public void SetName (string name)
	{
		this.name = name;
	}

	// -- Attack Damage --
	public int GetAttack ()
	{
		return attack;
	}
	public void SetAttack (int attack)
	{
		this.attack = attack;
	}

	// -- Health Point --
	public int GetHealth ()
	{
		return health;
	}
	public void SetHealth (int health)
	{
		this.health = health;
	}

	// -- Artwork --
	public Sprite GetArtwork ()
	{
		return artwork;
	}
	public void SetArtwork (Sprite artwork)
	{
		this.artwork = artwork;
	}

	// -- Cost --
	/*public int GetCost ()
	{
		return cost;
	}
	public void SetCost (int cost)
	{
		this.cost = cost;
	}*/

	// -- CardType --
	public CardType GetCardType ()
	{
		return cardType;
	}
	public void SetCardType (CardType cardType)
	{
		this.cardType = cardType;
	}

	// -----
	public int GetChance()
	{
		return chance;
	}

	public void SetChance(int chance)
	{
		this.chance = chance;
	}

	// Reset Method
	public virtual void ResetCard ()
	{
		name = "";
		attack = 0;
		health = 0;
		//cost = 0;
	}
}
