﻿using UnityEngine;

public class CardPilePicker : MonoBehaviour
{
	// References
	[SerializeField] private GameObject[] cardPiles;
	[SerializeField] private CardDrawer cd;

	public void PickCardPile (int index)
	{
		// Assign a new random seed
		Random.InitState (System.Environment.TickCount + index);
		Debug.Log ("Current random seed: " + Random.seed);

		// Dig into the game!
		GetComponent<Animator> ().SetBool ("cardPileSelected", true);
		Invoke ("DisableCardPilePanel", 1f);        // Can also stop the player to hover so early, game start delay
		Invoke ("DelayedStartRound", 1.3f);

		DrawCardsForPlayers ();
	}

	private void DisableCardPilePanel ()
	{
		gameObject.SetActive (false);
	}

	private void DelayedStartRound ()
	{
		FindObjectOfType<GameFlowController> ().StartRound ();
	}

	private void DrawCardsForPlayers ()
	{
		for (int i = 0; i < 3; i++)
		{
			cd.DrawCard (0);
			cd.DrawCard (1);
		}

		// Restrict dragging of cards
		Invoke ("RestrictCards", 0.1f);
	}

	private void RestrictCards ()
	{
		GameFlowController.cardRightControll ();
	}
}
