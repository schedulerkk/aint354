﻿using UnityEngine;
using UnityEngine.UI;

public class CardDisplayer : MonoBehaviour
{
	[Header ("References")]
	[SerializeField] private GameObject displayCard;

	// Internal variables
	private Animator displayCardAnimator;
	private GameObject spellBackground;
	private Image artwork;
	private Text attackText;
	private Text healthText;
	private GameObject elementFire;
	private GameObject elementIce;
	private GameObject elementPoison;
	private GameObject elementElectricity;

	private void Start ()
	{
		if (displayCard == null)
		{
			Debug.LogError ("Reference 'displayCard' not found");
			enabled = false;
			return;
		}

		displayCardAnimator = displayCard.GetComponent<Animator> ();
		spellBackground = displayCard.transform.Find ("Image_SpellBackground").gameObject;
		artwork = displayCard.transform.Find ("Image_Artwork").GetComponent<Image> ();
		attackText = displayCard.transform.Find ("Text_Attack").GetComponent<Text> ();
		healthText = displayCard.transform.Find ("Text_Health").GetComponent<Text> ();
		elementFire = displayCard.transform.Find ("Element_Fire").gameObject;
		elementIce = displayCard.transform.Find ("Element_Ice").gameObject;
		elementPoison = displayCard.transform.Find ("Element_Poison").gameObject;
		elementElectricity = displayCard.transform.Find ("Element_Electricity").gameObject;
	}

	public void DisplayCard (Card card)
	{
		Card tempCard = card;

		// - Reset all status from previous use -
		ResetObjects ();

		// - Setup -
		if (tempCard.GetCardType () == Card.CardType.Minion)
		{
			switch (tempCard.GetComponent<Minion> ().GetElement ())
			{
				case Minion.Element.Fire:
					elementFire.SetActive (true);
					break;
				case Minion.Element.Ice:
					elementIce.SetActive (true);
					break;
				case Minion.Element.Poison:
					elementPoison.SetActive (true);
					break;
				case Minion.Element.Electricity:
					elementElectricity.SetActive (true);
					break;
			}
		}
		else if (tempCard.GetCardType () == Card.CardType.Spell)
		{
			spellBackground.SetActive (true);
		}

		artwork.sprite = tempCard.GetArtwork ();
		attackText.text = tempCard.GetAttack ().ToString ();
		healthText.text = tempCard.GetHealth ().ToString ();

		// - Display card to player & Set hide timer -
		displayCard.SetActive (true);
		Invoke ("HideDisplayCard", 1.2f);
	}

	private void ResetObjects ()
	{
		CancelInvoke ();

		spellBackground.SetActive (false);

		elementFire.SetActive (false);
		elementIce.SetActive (false);
		elementPoison.SetActive (false);
		elementElectricity.SetActive (false);

		displayCard.SetActive (false);
	}

	private void HideDisplayCard ()
	{
		// Trigger animator
		displayCardAnimator.Play ("DisplayCard_Hide");

		CancelInvoke ();
		Invoke ("DisableDisplayCard", 0.3f);
	}

	private void DisableDisplayCard ()
	{
		displayCard.SetActive (false);
	}
}
