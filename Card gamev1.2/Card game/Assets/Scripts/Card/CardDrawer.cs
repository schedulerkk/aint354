﻿using UnityEngine;
using UnityEngine.UI;

public class CardDrawer : MonoBehaviour
{
	[Header ("Editable Variables")]
	[SerializeField] private int maxCardsInHand;

	[Header ("Object References")]
	[SerializeField] private ProceduralCardGenerator pcg;
	private Text UIText_attack;
	private Text UIText_health;
	private Image artwork;

	[Header ("Prefab References")]
	[SerializeField] private GameObject minionCardPrefab;
	[SerializeField] private GameObject spellCardPrefab;
	[SerializeField] private GameObject elementFire;
	[SerializeField] private GameObject elementIce;
	[SerializeField] private GameObject elementPoison;
	[SerializeField] private GameObject elementElectricity;

	public void DrawCard (int player)
	{
		if (GameObject.Find ("Panel_Hand_" + player).transform.childCount >= maxCardsInHand)
			return;

		Minion minionAttributes;
		Spell spellAttributes;

		// Create a new card object in scene
		GameObject card = null;

		// Determines the card type
		float cardType = Random.value;
		bool isMinion = (cardType > 1 - FindObjectOfType<ProceduralCardGenerator> ().minionCardPercentage);

		// Generate a new card with attributes
		if (isMinion)
		{
			minionAttributes = pcg.GenerateMinion (player);
			card = Instantiate (minionCardPrefab);

			// Assign values into the card
			Card generatedCard = card.GetComponent<Card> ();

			generatedCard.SetName (minionAttributes.GetName ());
			generatedCard.SetAttack (minionAttributes.GetAttack ());
			generatedCard.SetHealth (minionAttributes.GetHealth ());
			generatedCard.SetArtwork (minionAttributes.GetArtwork ());
			//generatedCard.SetCost (minionAttributes.GetCost ());
			generatedCard.SetCardType (minionAttributes.GetCardType ());

			// Link reference of the card object created
			artwork = card.transform.Find ("Image_Artwork").GetComponent<Image> ();
			UIText_attack = card.transform.Find ("Text_Attack").GetComponent<Text> ();
			UIText_health = card.transform.Find ("Text_Health").GetComponent<Text> ();

			// Assign attributes to the card object
			artwork.sprite = minionAttributes.GetArtwork ();
			UIText_attack.text = minionAttributes.GetAttack ().ToString ();
			UIText_health.text = minionAttributes.GetHealth ().ToString ();

			// Element attribute
			card.GetComponent<Minion> ().SetElement (minionAttributes.GetElement ());

			// Apply element to UI
			GameObject element;

			switch (minionAttributes.GetElement ())
			{
				case Minion.Element.Fire:
					element = GameObject.Instantiate (elementFire, card.transform.position, Quaternion.identity);
					element.transform.SetParent (card.transform);
					break;
				case Minion.Element.Ice:
					element = GameObject.Instantiate (elementIce, card.transform.position, Quaternion.identity);
					element.transform.SetParent (card.transform);
					break;
				case Minion.Element.Poison:
					element = GameObject.Instantiate (elementPoison, card.transform.position, Quaternion.identity);
					element.transform.SetParent (card.transform);
					break;
				case Minion.Element.Electricity:
					element = GameObject.Instantiate (elementElectricity, card.transform.position, Quaternion.identity);
					element.transform.SetParent (card.transform);
					break;

				default:
					break;
			}

			// Enable corresponding components
			card.GetComponent<Minion> ().enabled = true;
			card.GetComponent<MinionArrowMovement> ().enabled = true;
		}
		else
		{
			spellAttributes = pcg.GenerateSpell (player);
			card = Instantiate (spellCardPrefab);

			// Assign values into the card
			Card generatedCard = card.GetComponent<Card> ();

			generatedCard.SetName (spellAttributes.GetName ());
			generatedCard.SetAttack (spellAttributes.GetAttack ());
			generatedCard.SetHealth (spellAttributes.GetHealth ());
			generatedCard.SetArtwork (spellAttributes.GetArtwork ());
			//generatedCard.SetCost (spellAttributes.GetCost ());
			generatedCard.SetCardType (spellAttributes.GetCardType ());

			// Spell type attribute
			card.GetComponent<Spell> ().SetSpellType (spellAttributes.GetSpellType ());

			// Link reference of the card object created
			artwork = card.transform.Find ("Image_Artwork").GetComponent<Image> ();
			UIText_attack = card.transform.Find ("Text_Attack").GetComponent<Text> ();
			UIText_health = card.transform.Find ("Text_Health").GetComponent<Text> ();

			// Assign attributes to the card object
			artwork.sprite = spellAttributes.GetArtwork ();
			UIText_attack.text = spellAttributes.GetAttack ().ToString ();
			UIText_health.text = spellAttributes.GetHealth ().ToString ();

			// Enable corresponding components
			card.GetComponent<Spell> ().enabled = true;
		}

		// Attach card to the player's hand
		card.transform.SetParent (GameObject.Find ("Panel_Hand_" + player).transform, false);
	}
}
