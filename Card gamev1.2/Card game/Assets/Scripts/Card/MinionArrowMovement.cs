﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MinionArrowMovement : MonoBehaviour, IPointerClickHandler
{
	[Header ("References")]
	[SerializeField] private GameObject arrowPrefab;

	public static bool arrowsDisplaying;           // Determines whether the arrow are currently displaying to the player
	public static GameObject frontArrow;           // Refers to front arrow, with inital value null
	public static GameObject backArrow;        // Refers to back arrow, with inital value null
	public static GameObject middleArrow;      // Refers to middle arrow, with inital value null
	public static bool createArrows;   // Makes sure the arrows are created once only, since there are lots of cards that attached this script in scene

	public static GameObject currentSelectedMinion;     // Represents the current selected minion, ready for moving

	void Start ()
	{
		// Create arrow buttons if they are not yet in scene
		if (!createArrows)
		{
			Transform canvas = GameObject.Find ("Canvas").transform;

			frontArrow = Instantiate (arrowPrefab, canvas);
			backArrow = Instantiate (arrowPrefab, canvas);
			middleArrow = Instantiate (arrowPrefab, canvas);

			// Push the arrows in front of all other canvas elements
			//PushSortingLayer ();		// Seems like adding a canvas component will affect the button being clicked??

			// Setup onClick events
			ButtonManager bm = GameObject.Find ("Canvas").GetComponent<ButtonManager> ();
			frontArrow.GetComponent<Button> ().onClick.AddListener (delegate { bm.MoveMinionByArrow (1); });
			backArrow.GetComponent<Button> ().onClick.AddListener (delegate { bm.MoveMinionByArrow (-1); });
			middleArrow.GetComponent<Button> ().onClick.AddListener (delegate { bm.MoveMinionByArrow (0); });

			// Assign arrow object names in scene just for easy debugging =)
			frontArrow.gameObject.name = "Button_FrontArrow";
			backArrow.gameObject.name = "Button_BackArrow";
			middleArrow.gameObject.name = "Button_MiddleArrow";

			// All settings done, hide the arrows from players
			frontArrow.SetActive (false);
			backArrow.SetActive (false);
			middleArrow.SetActive (false);

			createArrows = true;
		}
	}

	private void PushSortingLayer ()
	{
		frontArrow.GetComponent<Canvas> ().overrideSorting = true;
		frontArrow.GetComponent<Canvas> ().sortingOrder = 2;
		backArrow.GetComponent<Canvas> ().overrideSorting = true;
		backArrow.GetComponent<Canvas> ().sortingOrder = 2;
	}

	private void DisplayMovementOptions ()
	{
		// Players should not be able to move AI's card
		if (GetComponent<Card> ().cardRef != 0)
			return;

		arrowsDisplaying = true;

		// -- Get current field --
		string fieldName = transform.parent.name;
		int currentPathSet = 0;         // 0-Halo, 1-PathA, 2-PathB
		if (fieldName.Contains ("a"))
			currentPathSet = 1;
		else if (fieldName.Contains ("b"))
			currentPathSet = 2;
		// - Field number -
		string _fieldNum = transform.parent.name;
		string fieldNum = string.Empty;
		for (int i = 0; i < _fieldNum.Length; i++)
		{
			if (char.IsDigit (_fieldNum [i]))
			{
				fieldNum += _fieldNum [i];
			}
		}
		int currentFieldNum = int.Parse (fieldNum);

		// Variables that indicate the arrows' positions have been fixed
		bool frontArrowFixPosition = false;
		bool backArrowFixPosition = false;

		// -- Display arrow buttons to different directions --
		FieldController _fieldController = FindObjectOfType<FieldController> ();
		int frontArrowIndex = currentFieldNum + 1;
		int backArrowIndex = currentFieldNum - 1;

		// Get a small fix on placing arrows when card is on the edge of middle fields to halo
		if (currentPathSet == 1)
		{
			if (currentFieldNum == 0)
			{
				backArrowFixPosition = true;
				backArrowIndex = 4;
			}
			else if (currentFieldNum == 4)
			{
				frontArrowFixPosition = true;
				frontArrowIndex = 10;
			}
		}
		else if (currentPathSet == 2)
		{
			if (currentFieldNum == 0)
			{
				backArrowFixPosition = true;
				backArrowIndex = 18;
			}
			else if (currentFieldNum == 4)
			{
				frontArrowFixPosition = true;
				frontArrowIndex = 24;
			}
		}

		// - Middle arrows -
		int middleArrowPathId;      // Indicates which/whether middle arrrow will be allowed in path set [0-Halo, 1-PathA, 2-PathB]
		int middleArrowIndex;
		if (fieldName.Equals ("4"))
		{
			middleArrowPathId = 1;
			middleArrowIndex = 0;
		}
		else if (fieldName.Equals ("10"))
		{
			middleArrowPathId = 1;
			middleArrowIndex = 4;
		}
		else if (fieldName.Equals ("18"))
		{
			middleArrowPathId = 2;
			middleArrowIndex = 0;
		}
		else if (fieldName.Equals ("24"))
		{
			middleArrowPathId = 2;
			middleArrowIndex = 4;
		}
		else
		{
			middleArrowPathId = 0;
			middleArrowIndex = -1;
		}

		// - Prevent index array out of bound if the arrows will reach the start/end -
		// Middle path scenerios
		if (currentPathSet == 1)
		{
			if (frontArrowIndex > FindObjectOfType<FieldController> ().middleFieldsA.Length - 1 && !frontArrowFixPosition)
			{
				frontArrowFixPosition = true;
				frontArrowIndex = 11;
			}

			if (backArrowIndex < 0 && !backArrowFixPosition)
			{
				backArrowFixPosition = true;
				backArrowIndex = 5;
			}
		}
		else if (currentPathSet == 2)
		{
			if (frontArrowIndex > FindObjectOfType<FieldController> ().middleFieldsB.Length - 1 && !frontArrowFixPosition)
			{
				frontArrowFixPosition = true;
				frontArrowIndex = 25;
			}

			if (backArrowIndex < 0 && !backArrowFixPosition)
			{
				backArrowFixPosition = true;
				backArrowIndex = 19;
			}
		}
		else        // Halo path scenerio
		{
			if (frontArrowIndex > _fieldController.fieldList.Count - 1)
			{
				frontArrowIndex = 0;
			}

			if (backArrowIndex < 0)
			{
				backArrowIndex = _fieldController.fieldList.Count - 1;
			}
		}

		// Align arrows with field
		string frontFieldName = (currentPathSet == 1 && !frontArrowFixPosition ? "a" : currentPathSet == 2 && !frontArrowFixPosition ? "b" : "") + frontArrowIndex.ToString ();
		string backFieldName = (currentPathSet == 1 && !backArrowFixPosition ? "a" : currentPathSet == 2 && !backArrowFixPosition ? "b" : "") + backArrowIndex.ToString ();

		/*frontArrow.transform.SetParent (GameObject.Find (frontArrowIndex.ToString ()).transform);
		backArrow.transform.SetParent (GameObject.Find (backArrowIndex.ToString ()).transform);*/
		// Bug Fix: arrow set parent to field will affect by field horizontal group --> layout
		frontArrow.transform.position = GameObject.Find (frontFieldName).transform.position;
		backArrow.transform.position = GameObject.Find (backFieldName).transform.position;
		if (middleArrowPathId == 1)
			middleArrow.transform.position = GameObject.Find ("a" + middleArrowIndex.ToString ()).transform.position;
		else if (middleArrowPathId == 2)
			middleArrow.transform.position = GameObject.Find ("b" + middleArrowIndex.ToString ()).transform.position;

		// - Rotate Arrows -
		RotateArrows (currentFieldNum, currentPathSet);

		// - Display them to player -
		frontArrow.SetActive (true);
		backArrow.SetActive (true);
		middleArrow.SetActive (middleArrowPathId == 0 ? false : true);
	}

	private void RotateArrows (int currentField, int currentPathSet)
	{
		int fieldSector = currentField / 7;     // 28 fields in game, gives out fieldSector = 0/1/2/3, used for halo only

		// Define rotations
		Quaternion rotation_Up = Quaternion.Euler (0f, 0f, 0f);
		Quaternion rotation_Left = Quaternion.Euler (0f, 0f, 90f);
		Quaternion rotation_Down = Quaternion.Euler (0f, 0f, 180f);
		Quaternion rotation_Right = Quaternion.Euler (0f, 0f, 270f);

		RectTransform cardRectTrans = this.GetComponent<RectTransform> ();

		RectTransform frontArrowRectTrans = frontArrow.GetComponent<RectTransform> ();
		RectTransform backArrowRectTrans = backArrow.GetComponent<RectTransform> ();
		RectTransform middleArrowRectTrans = middleArrow.GetComponent<RectTransform> ();

		if (currentPathSet == 0)        // Halo path
		{
			switch (fieldSector)
			{
				case 0:
					if (currentField == 0)      // Current field is a corner, redirect arrows
					{
						frontArrowRectTrans.rotation = rotation_Left;
						backArrowRectTrans.rotation = rotation_Up;
						break;
					}

					if (currentField == 4)
					{
						middleArrowRectTrans.rotation = rotation_Up;
					}

					frontArrowRectTrans.rotation = rotation_Left;
					backArrowRectTrans.rotation = rotation_Right;
					break;
				case 1:
					if (currentField == 7)      // Current field is a corner, redirect arrows
					{
						frontArrowRectTrans.rotation = rotation_Up;
						backArrowRectTrans.rotation = rotation_Right;
						break;
					}

					if (currentField == 10)     // Middle path
					{
						middleArrowRectTrans.rotation = rotation_Right;
					}

					frontArrowRectTrans.rotation = rotation_Up;
					backArrowRectTrans.rotation = rotation_Down;
					break;
				case 2:
					if (currentField == 14)     // Current field is a corner, redirect arrows
					{
						frontArrowRectTrans.rotation = rotation_Right;
						backArrowRectTrans.rotation = rotation_Down;
						break;
					}

					if (currentField == 18)     // Middle path
					{
						middleArrowRectTrans.rotation = rotation_Down;
					}

					frontArrowRectTrans.rotation = rotation_Right;
					backArrowRectTrans.rotation = rotation_Left;
					break;
				case 3:
					if (currentField == 21)     // Current field is a corner, redirect arrows
					{
						frontArrowRectTrans.rotation = rotation_Down;
						backArrowRectTrans.rotation = rotation_Left;
						break;
					}

					if (currentField == 24)     // Middle path
					{
						middleArrowRectTrans.rotation = rotation_Left;
					}

					frontArrowRectTrans.rotation = rotation_Down;
					backArrowRectTrans.rotation = rotation_Up;
					break;
			}
		}
		else if (currentPathSet == 1)        // Middle Path A
		{
			if (currentField < 2)
			{
				frontArrowRectTrans.rotation = rotation_Up;
				backArrowRectTrans.rotation = rotation_Down;
			}
			else if (currentField == 2)     // Corner
			{
				frontArrowRectTrans.rotation = rotation_Left;
				backArrowRectTrans.rotation = rotation_Down;
			}
			else
			{
				frontArrowRectTrans.rotation = rotation_Left;
				backArrowRectTrans.rotation = rotation_Right;
			}
		}
		else if (currentPathSet == 2)        // Middle Path B
		{
			if (currentField < 2)
			{
				frontArrowRectTrans.rotation = rotation_Down;
				backArrowRectTrans.rotation = rotation_Up;
			}
			else if (currentField == 2)      // Corner
			{
				frontArrowRectTrans.rotation = rotation_Right;
				backArrowRectTrans.rotation = rotation_Up;
			}
			else
			{
				frontArrowRectTrans.rotation = rotation_Right;
				backArrowRectTrans.rotation = rotation_Left;
			}
		}
	}

	public static void HideMovementOptions ()
	{
		if (frontArrow.activeSelf)
		{
			frontArrow.SetActive (false);
		}

		if (backArrow.activeSelf)
		{
			backArrow.SetActive (false);
		}

		if (middleArrow.activeSelf)
		{
			middleArrow.SetActive (false);
		}

		arrowsDisplaying = false;
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		// Error Message Fix: arrows not to be displayed to players when the cards are still in their hands
		if (transform.parent.name.Equals ("Panel_Hand_0") || transform.parent.name.Equals ("Panel_Hand_1"))
		{
			return;
		}

		// Bug Fix: Player not be able to show/hide movement arrows when this is not his current round
		if (GameFlowController.playerTurn != 0)
		{
			return;
		}

		if (!arrowsDisplaying)
		{
			// Error Handling: Only display movement option after rolling the dice
			if (!GameFlowController.isRolled || GameFlowController.isMoved)
			{
				return;
			}

			DisplayMovementOptions ();
			currentSelectedMinion = this.gameObject;
		}
		else
		{
			HideMovementOptions ();
			currentSelectedMinion = null;
		}
	}
}
