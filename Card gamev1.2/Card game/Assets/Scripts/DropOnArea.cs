﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropOnArea : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	//public Drag.Slot typeOfCard = Drag.Slot.MINION;		// Declare which type can be drop in this field
	public int hasMinionCard;       // Defines whether the field has a minion (-1 None, 0 player, 1 AI)
	public bool hasZoneCard;

	void Start ()
	{
		hasMinionCard = -1;
		hasZoneCard = false;
	}

	public void OnPointerEnter (PointerEventData ped)
	{
		//Debug.Log("OnPointerEnter");

		Animator animator = this.GetComponent<Animator> ();
		if (animator != null)
		{
			animator.SetBool ("popUp", true);
		}

		if (ped.pointerDrag == null)
			return;

		Card c = ped.pointerDrag.GetComponent<Card> ();
		if (c != null)
		{
			c.cardPlaceParent = this.transform;
		}
	}

	public void OnPointerExit (PointerEventData ped)
	{
		//Debug.Log("OnPointerExit");

		Animator animator = this.GetComponent<Animator> ();
		if (animator != null)
		{
			animator.SetBool ("popUp", false);
		}

		if (ped.pointerDrag == null)
			return;

		Card c = ped.pointerDrag.GetComponent<Card> ();

		if (c != null && c.cardPlaceParent == this.transform)
		{
			c.cardPlaceParent = this.transform;
		}
	}

	public void OnDrop (PointerEventData ped)
	{
		//Debug.Log (ped.pointerDrag.name + " dropped to " + gameObject.name);		// PointerDrag: the thing I dragging

		Minion minion = ped.pointerDrag.GetComponent<Minion> ();

		if (minion != null)
		{
			// Check if the card is allowed to be dropped to this field
			if (hasMinionCard == -1)
			{
				if (this.transform == minion.targetedfield_1.transform ||
					this.transform == minion.targetedfield_2.transform)
				{
					ped.pointerDrag.GetComponent<Minion> ().parentReturnto = this.transform;       // Move card to the new field

					//GameFlowController.isMoved = true;

				}
			}
		}
	}
}
