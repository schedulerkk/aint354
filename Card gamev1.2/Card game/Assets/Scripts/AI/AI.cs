﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AI : MonoBehaviour
{
	[SerializeField] private ButtonManager buttonManager;
	[SerializeField] private GameFlowController gameFlowController;
	[SerializeField] private FieldController fieldController;
	[SerializeField] private GameObject hands;
	private bool isEven;
	private GameObject chosenMinion;
	private int _fieldNum, fieldNum;
	public static bool forward;
	public static bool hasAIAlready;
	private bool isMoveCompleted;
	private bool isSummonCompleted;
	/* For Spell Moving */
	private List<GameObject> spellPool;
	private int chance_inSpellPool;
	private GameObject target_Spell;
	private bool isSpellMoving;
	private bool isSpellEffectCompleted;
	/* For AI move in middle path */
	GameObject chosenMinion_mPathA, chosenMinion_mPathB;
	List<GameObject> mPathPool_A, mPathPool_B;
	int _step_mPath;
	int mPathIndex;
	GameObject targetedfield_mPath = null;

	void Update ()
	{
		SpellAction (spellPool, chance_inSpellPool, target_Spell);
	}

	public IEnumerator RunAI ()
	{

		// 2. Check if it is moved.
		if (GameFlowController.isMoved == false)
		{
			// Make movement.

			// Check if there is minion in the field, if no, summom a minion
			if (!AreAnyMinionsOnField () && hasAIAlready == false &&
			 	fieldController.fieldList [14].GetComponent<DropOnArea> ().hasMinionCard == -1)
			{
				// - Idle -
				Invoke("Waiting", 1.5f);
				while(IsInvoking())
					yield return null;
				// - Summon -
				Invoke ("Summon", 0.2f);
				GameFlowController.isRolled = true;
				while (IsInvoking ())
					yield return null;
				Invoke("Waiting", 1f);
					while(IsInvoking())
						yield return null;
				// - Attack -
				Attack (true);
				Attack (false);
			}
			else                                        // if yes, make movement
			{
				if (!hasAIAlready)
				{
					// - Idle  -
					Invoke("Waiting", 1.5f);
					while(IsInvoking())
						yield return null;
					// - Spell using -
					UseSpell ();
					Invoke("Waiting", 1f);
					while(IsInvoking())
						yield return null;
					// - Roll, Move and Attack -
					Roll ();
					StartCoroutine (Move ());
					while (isMoveCompleted == false)
						yield return null;
					Attack (forward);
					Debug.Log ("direction  " + forward);
					GameFlowController.isRolled = true;
					Debug.Log ("isRolled 1 : " + GameFlowController.isRolled);
					// - Idle -
					Invoke("Waiting", 1f);
					while(IsInvoking())
						yield return null;
				}
			}
		}
		Debug.Log ("isRolled 2 : " + GameFlowController.isRolled);
		//Debug.Log("###" + hasAIAlready);
		// 3. End turn.
		if (GameFlowController.isRolled == true)
		{
			Debug.Log ("isMove : " + GameFlowController.isMoved);
			Debug.Log ("isRolled 3 : " + GameFlowController.isRolled);

			if (!GameFlowController.isGameOver)
				buttonManager.EndTurn ();
		}
	}

	protected void Roll ()
	{
		buttonManager.RollDice ();
		isEven = ButtonManager.dice_number % 2 == 0;
		Debug.Log ("AI : Phase Roll() Done.");
	}

	protected bool AreAnyMinionsOnField ()
	{
		int minionCount = 0, enemyCount = 0;
		int summonChange = Random.Range (1, 101);
		for (int i = 0; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				minionCount += 1;
			}
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				enemyCount += 1;
			}
		}
		if (minionCount != 0)
		{
			if (minionCount > enemyCount)
			{
				if (summonChange < 20)
				{
					Debug.Log ("20: summon change is " + summonChange);
					Debug.Log ("minionCount : " + minionCount);
					Debug.Log ("enemyCount : " + enemyCount);
					return false;
				}
				return true;
			}
			else if (minionCount <= enemyCount)
			{
				if (summonChange < 80)
				{
					Debug.Log ("80: summon change is " + summonChange);
					Debug.Log ("minionCount : " + minionCount);
					Debug.Log ("enemyCount : " + enemyCount);
					return false;
				}
				return true;
			}
		}

		return false;
	}

	protected void Summon ()
	{
		isSummonCompleted = false;
		// 1. Check if there are any minions in hand.
		List<GameObject> minionPool = new List<GameObject> ();
		for (int i = 0; i < hands.transform.childCount; i++)
		{
			if (hands.transform.GetChild (i).GetComponent<Minion> () != null)   // check if it is minion
				minionPool.Add (hands.transform.GetChild (i).gameObject);
		}
		// 2. Pick the highest ATK value one.
		int highest;
		chosenMinion = minionPool [0];

		highest = minionPool [0].GetComponent<Minion> ().GetAttack ();
		for (int i = 1; i < minionPool.Count; i++)
		{
			if (minionPool [i].GetComponent<Minion> ().GetAttack () > highest)
			{
				highest = minionPool [i].GetComponent<Minion> ().GetAttack ();
				chosenMinion = minionPool [i];
			}
		}

		// 3. Summon.
		GameObject targetedfield_1, targetedfield_2;
		targetedfield_1 = fieldController.fieldList [14];
		targetedfield_2 = targetedfield_1;

		Minion minion = chosenMinion.GetComponent<Minion> ();
		minion.parentReturnto = targetedfield_1.transform;
		chosenMinion.transform.SetParent (minion.parentReturnto);
		targetedfield_1.GetComponent<DropOnArea> ().hasMinionCard = 1;
		GameFlowController.isMoved = true;
		chosenMinion.GetComponent<Minion> ().location = 14;
		Debug.Log ("AI : Phase Summon() Done.");
		isSummonCompleted = true;
		FindObjectOfType<CardDisplayer> ().DisplayCard (chosenMinion.GetComponent<Card> ());
	}

	protected IEnumerator Move ()
	{
		isMoveCompleted = false;
		GameObject targetedfield_1 = null, targetedfield_2 = null, targetedField = null;
		targetedfield_mPath = null;
		chosenMinion_mPathA = null;
		chosenMinion_mPathB = null;
		hasAIAlready = true;
		// 1. Get all existed AI's minions into a list.
		List<GameObject> minionPool = new List<GameObject> ();
		mPathPool_A = new List<GameObject> ();
		mPathPool_B = new List<GameObject> ();
		for (int i = 0; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				minionPool.Add (fieldController.fieldList [i].transform.GetChild (0).gameObject);
			}
		}
		// Extra 1 : Get all existed AI in Middle Path in list.
		/*
		for (int i = 0; i < fieldController.mPathList_A.Count; i++)
		{
			if (fieldController.mPathList_A [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				mPathPool_A.Add (fieldController.mPathList_A [i].transform.GetChild (0).gameObject);
			}
		}
		for (int i = 0; i < fieldController.mPathList_B.Count; i++)
		{
			if (fieldController.mPathList_B [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				mPathPool_B.Add (fieldController.mPathList_B [i].transform.GetChild (0).gameObject);
			}
		}
		
		// Extra 2 : Decide whether AI go into middle path.
		Evaluation_EntryMPath();

		// Extra 3 : Check the step in Middle Path.
		if(chosenMinion_mPathA != null)
		{
			targetedfield_mPath = CheckMpathStep(chosenMinion_mPathA);
		}
		else if(chosenMinion_mPathB != null)
		{
			targetedfield_mPath = CheckMpathStep(chosenMinion_mPathB);
		}
		else if(chosenMinion_mPathA == null && chosenMinion_mPathB == null)
		{
			print("No any minions can be considered for middle path operation...");
		}
		else
		{
			Debug.Log("Something Go Wrong here...");
		}*/


		for (int j = 0; j < minionPool.Count; j++)
		{
			// 2. Check if there anything block the field
			Minion minion = minionPool [j].GetComponent<Minion> ();
			string fieldString = minion.parentReturnto.name;
			minion.stepForward = ButtonManager.dice_number;
			minion.stepBackward = ButtonManager.dice_number;

			int.TryParse (fieldString, out _fieldNum);
			fieldNum = _fieldNum;                                           //original location

			for (int i = 1; i <= ButtonManager.dice_number; i++)
			{
				if (fieldNum + i > fieldController.fieldList.Count - 1)
				{
					if (fieldController.fieldList [fieldNum + i - fieldController.fieldList.Count].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					fieldController.fieldList [fieldNum + i - fieldController.fieldList.Count].GetComponent<DropOnArea> ().hasMinionCard == 1)
					{
						minion.stepForward = i - 1;
						break;
					}
				}
				else
				{
					if (fieldController.fieldList [fieldNum + i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					fieldController.fieldList [fieldNum + i].GetComponent<DropOnArea> ().hasMinionCard == 1)
					{
						minion.stepForward = i - 1;
						break;
					}
				}
			}

			for (int i = 1; i <= ButtonManager.dice_number; i++)
			{
				if (fieldNum - i < 0)
				{
					if (fieldController.fieldList [fieldController.fieldList.Count - i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					fieldController.fieldList [fieldController.fieldList.Count - i].GetComponent<DropOnArea> ().hasMinionCard == 1)
					{
						minion.stepBackward = i - 1;
						break;
					}
				}
				else
				{
					if (fieldController.fieldList [fieldNum - i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					fieldController.fieldList [fieldNum - i].GetComponent<DropOnArea> ().hasMinionCard == 1)
					{
						minion.stepBackward = i - 1;
						break;
					}
				}
			}
			Debug.Log ("forward : " + minion.stepForward + " backward : " + minion.stepBackward);
			if (fieldNum == 0)
			{
				targetedfield_1 = fieldController.fieldList [minion.stepForward];
				targetedfield_2 = fieldController.fieldList [fieldController.fieldList.Count - minion.stepBackward];
			}
			else if (fieldNum + minion.stepForward >= fieldController.fieldList.Count)
			{
				int _temp_TargetedField = fieldNum + minion.stepForward - fieldController.fieldList.Count;
				targetedfield_1 = fieldController.fieldList [_temp_TargetedField];
				targetedfield_2 = fieldController.fieldList [fieldNum - minion.stepBackward];
			}
			else
			{
				targetedfield_1 = fieldController.fieldList [fieldNum + minion.stepForward];
				//Debug.Log("" + fieldNum + " " + diceNumber );
				if (fieldNum - minion.stepBackward < 0)
				{
					int _temp_TargetedField = fieldController.fieldList.Count + (fieldNum - minion.stepBackward);
					targetedfield_2 = fieldController.fieldList [_temp_TargetedField];
				}
				else
					targetedfield_2 = fieldController.fieldList [fieldNum - minion.stepBackward];
				//Debug.Log("" + fieldNum + " " + diceNumber );
			}

			minion.temp_tf = targetedField;
			minion.temp_tf1 = targetedfield_1;
			minion.temp_tf2 = targetedfield_2;
			// 4.2 Evaluation
			Evaluation_Minions (minion, minionPool, minion.stepForward, minion.stepBackward);

		}
		// for loop end here.

		// Pick the most front on each path, Compare the value
		chosenMinion = minionPool [0];
		GameObject candidate_top = null;
		GameObject candidate_bot = null;

		// Top Path:
		for (int i = 15; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				candidate_top = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}

		// Bottom Path:
		for (int i = 13; i > 1; i--)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				candidate_bot = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}

		// Compare these two value, if they can not NULL
		// If one of them is null, it means just there is just ONE path.
		// The situation that "No minions on field" is not considered in here.
		if (candidate_top != null && candidate_bot != null)
		{
			int chanceInBoth = candidate_top.GetComponent<Minion> ().GetChance () + candidate_bot.GetComponent<Minion> ().GetChance ();
			int r_chanceInAll = Random.Range (1, chanceInBoth);
			if (r_chanceInAll < candidate_top.GetComponent<Minion> ().GetChance ())
			{
				chosenMinion = candidate_top;
			}
			else
			{
				chosenMinion = candidate_bot;
			}
			/*
			if (candidate_top.GetComponent<Minion> ().GetChance () > candidate_bot.GetComponent<Minion> ().GetChance ())
			{
				chosenMinion = candidate_top;
			}
			else if (candidate_top.GetComponent<Minion> ().GetChance () < candidate_bot.GetComponent<Minion> ().GetChance ())
			{
				chosenMinion = candidate_bot;
			}
			else                //if same chance, random.
			{
				int randomChance = Random.Range (1, 101);
				if (randomChance < 50)
					chosenMinion = candidate_top;
				else
					chosenMinion = candidate_bot;
			}*/
		}
		else                    //There is just one path that has minion
		{
			if (candidate_top != null)
				chosenMinion = candidate_top;
			if (candidate_bot != null)
				chosenMinion = candidate_bot;
		}

		Debug.Log ("Chosen's Name : " + chosenMinion.transform.parent.name);

		// 5.2 Check nearest enemy
		targetedField = NearestEnemy (chosenMinion.GetComponent<Minion> ().temp_tf,
									chosenMinion.GetComponent<Minion> ().temp_tf1,
									chosenMinion.GetComponent<Minion> ().temp_tf2,
									chosenMinion.GetComponent<Minion> ().location);

		// 5.3 Replace the chosenMinion if the mPath is chosen
		/*
		if(chosenMinion_mPathA != null)
		{
			// The targetedField has to be reconsidered in here
			chosenMinion_mPathA.transform.parent.GetComponent<DropOnArea>().hasMinionCard = -1;
			Debug.Log("targetedfield_mPath : " + targetedfield_mPath.name);
			chosenMinion_mPathA.GetComponent<Minion>().parentReturnto = targetedfield_mPath.transform;

			if(chosenMinion_mPathA.transform.parent.name == "10")
			{
				for(int i=0;i<_step_mPath;i++)
				{
					if(i == 5)
						chosenMinion_mPathA.transform.SetParent(fieldController.fieldList[4].transform);
					else
						chosenMinion_mPathA.transform.SetParent(fieldController.mPathList_A[i].transform);

					if (i < _step_mPath) yield return new WaitForSeconds (0.5f);
					if(i == 5) break;
				}
			}
			else
			{
				for(int i=mPathIndex;i<_step_mPath;i++)
				{
					if(i>4)
						chosenMinion_mPathA.transform.SetParent(fieldController.fieldList[4].transform);
					else
						chosenMinion_mPathA.transform.SetParent(fieldController.mPathList_A[i].transform);

					if (i < _step_mPath) yield return new WaitForSeconds (0.5f);
					if(i > 4) break;
				}
			}
			targetedfield_mPath.GetComponent<DropOnArea> ().hasMinionCard = 1;
			GameFlowController.isMoved = true;
			Debug.Log ("AI : Phase Move() Done.");
			hasAIAlready = false;
			isMoveCompleted = true;
		}
		else if(chosenMinion_mPathB != null)
		{
			// The targetedField has to be reconsidered in here
			chosenMinion_mPathB.transform.parent.GetComponent<DropOnArea>().hasMinionCard = -1;
			chosenMinion_mPathB.GetComponent<Minion>().parentReturnto = targetedfield_mPath.transform;

			if(chosenMinion_mPathB.transform.parent.name == "18")
			{
				for(int i=0;i<_step_mPath;i++)
				{
					if(i == 5)
						chosenMinion_mPathB.transform.SetParent(fieldController.fieldList[24].transform);
					else
						chosenMinion_mPathB.transform.SetParent(fieldController.mPathList_B[i].transform);
					if (i < _step_mPath) yield return new WaitForSeconds (0.5f);
					if(i == 5)	break;
				}
			}
			else
			{
				for(int i=mPathIndex;i<_step_mPath;i++)
				{
					if(i > 4)
						chosenMinion_mPathB.transform.SetParent(fieldController.fieldList[24].transform);
					else
						chosenMinion_mPathB.transform.SetParent(fieldController.mPathList_B[i].transform);
					if (i < _step_mPath) yield return new WaitForSeconds (0.5f);
					if(i > 4)	break;
				}
			}
			targetedfield_mPath.GetComponent<DropOnArea> ().hasMinionCard = 1;
			GameFlowController.isMoved = true;
			Debug.Log ("AI : Phase Move() Done.");
			hasAIAlready = false;
			isMoveCompleted = true;
			
		}
		// 6. Move
		else if(chosenMinion_mPathA == null && chosenMinion_mPathB == null)
		{
			chosenMinion.transform.parent.GetComponent<DropOnArea> ().hasMinionCard = -1;
			chosenMinion.GetComponent<Minion>().parentReturnto = targetedField.transform;
			// Moving animation
			string origin_fieldNum_string = chosenMinion.transform.parent.name;
			int ofs, origin_fieldNum;
			int.TryParse(origin_fieldNum_string, out ofs);
			origin_fieldNum = ofs;

			int tf, tfNum, step;
			int.TryParse (targetedField.transform.name, out tf);
			tfNum = tf;
			step = tfNum - origin_fieldNum;
			Debug.Log("tfNum : " + tfNum);
			Debug.Log("origin_fieldNum" + origin_fieldNum);
			Debug.Log("step 1 : " + step);
			step = (step < 0) ? step * -1 : step;
			Debug.Log("AI : " + step);
			for (int i = 1; i <= step; i++)
			{	
				if (forward)
				{
					chosenMinion.transform.SetParent (fieldController.fieldList [origin_fieldNum + i].transform);
				}
				else
				{
					chosenMinion.transform.SetParent (fieldController.fieldList [origin_fieldNum - i].transform);
				}
				if (i <= step) yield return new WaitForSeconds (0.5f);
			}
			targetedField.GetComponent<DropOnArea> ().hasMinionCard = 1;

			chosenMinion.GetComponent<Minion>().location = tfNum;
			GameFlowController.isMoved = true;
			Debug.Log ("AI : Phase Move() Done.");
			hasAIAlready = false;
			isMoveCompleted = true;
		}
		else
		{
			Debug.Log("Something wrong here.");
		}
		*/
		chosenMinion.transform.parent.GetComponent<DropOnArea> ().hasMinionCard = -1;
		chosenMinion.GetComponent<Minion> ().parentReturnto = targetedField.transform;
		// Moving animation
		string origin_fieldNum_string = chosenMinion.transform.parent.name;
		int ofs, origin_fieldNum;
		int.TryParse (origin_fieldNum_string, out ofs);
		origin_fieldNum = ofs;

		int tf, tfNum, step;
		int.TryParse (targetedField.transform.name, out tf);
		tfNum = tf;
		step = tfNum - origin_fieldNum;
		Debug.Log ("tfNum : " + tfNum);
		Debug.Log ("origin_fieldNum" + origin_fieldNum);
		Debug.Log ("step 1 : " + step);
		step = (step < 0) ? step * -1 : step;
		Debug.Log ("AI : " + step);
		for (int i = 1; i <= step; i++)
		{


			if (forward)
			{
				if (origin_fieldNum + i >= fieldController.fieldList.Count)
				{
					chosenMinion.transform.SetParent (fieldController.fieldList [0].transform);
					gameFlowController.WinGame (1);
				}
				else
					chosenMinion.transform.SetParent (fieldController.fieldList [origin_fieldNum + i].transform);
			}
			else
			{
				if (origin_fieldNum - i <= 0)
				{
					chosenMinion.transform.SetParent (fieldController.fieldList [0].transform);
					gameFlowController.WinGame (1);
				}
				else
					chosenMinion.transform.SetParent (fieldController.fieldList [origin_fieldNum - i].transform);
			}

			// Play audio effect
			if (!GameFlowController.isGameOver)
			{
				FindObjectOfType<SoundManager> ().PlaySound ("minionStep");
			}
			else
			{
				FindObjectOfType<SoundManager> ().PlaySound ("lose");
				break;
			}

			if (i <= step)
				yield return new WaitForSeconds (0.5f);


		}
		targetedField.GetComponent<DropOnArea> ().hasMinionCard = 1;

		chosenMinion.GetComponent<Minion> ().location = tfNum;
		GameFlowController.isMoved = true;
		Debug.Log ("AI : Phase Move() Done.");
		hasAIAlready = false;
		isMoveCompleted = true;

	}

	protected void Attack (bool directionFront)
	{
		chosenMinion.GetComponent<Minion> ().Attack (directionFront);
	}

	GameObject NearestEnemy (GameObject targetedField, GameObject targetedfield_1, GameObject targetedfield_2, int location)
	{
		int nearestEnemy_forward = 0, nearestEnemy_backward = 0;
		for (int i = 0; i < 14; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				nearestEnemy_backward = i;
			}
		}
		for (int i = 27; i > 14; i--)
		{
			if (i == 27)
			{

				if (fieldController.fieldList [0].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					nearestEnemy_forward = 0;
				}

			}
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				nearestEnemy_forward = i;
			}

			// if no enemy in top path, set the "forward" to 28 for calculation
			if (i == 15 && nearestEnemy_forward == 0)
			{
				nearestEnemy_forward = fieldController.fieldList.Count;
			}
		}
		// if no enemy, keep going
		for (int i = 0; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				break;
			if (i == fieldController.fieldList.Count - 1)
			{
				if (fieldNum <= 14)// in 0-14 -> go backward
				{
					forward = false;
					return targetedField = targetedfield_2;
				}
				else
				{
					forward = true;
					return targetedField = targetedfield_1;
				}
			}
		}

		// Compare which is nearest
		nearestEnemy_forward = fieldController.fieldList.Count - nearestEnemy_forward;
		Debug.Log ("backward : " + nearestEnemy_backward + " forward : " + nearestEnemy_forward);


		if (nearestEnemy_backward > nearestEnemy_forward)
		{
			forward = false;
			targetedField = targetedfield_2;
			if (location > 14)
			{
				forward = true;
				targetedField = targetedfield_1;
			}
			else if (location < 14)
			{
				forward = false;
				targetedField = targetedfield_2;
				Debug.Log ("location " + location);
			}
		}
		else
		{
			forward = true;
			targetedField = targetedfield_1;
			if (location > 14)
			{
				forward = true;
				targetedField = targetedfield_1;
			}
			else if (location < 14)
			{
				forward = false;
				targetedField = targetedfield_2;
				Debug.Log ("location " + location);
			}
		}

		return targetedField;
	}

	void Evaluation_Minions (Minion minion, List<GameObject> minionPool, int stepForward, int stepBackward)
	{
		List<GameObject> enemyPool_back = new List<GameObject> ();
		Minion _enemy;
		int chance = 0;
		// Case 1 : If it can get win directly - 100%
		//if(fieldNum + stepForward >= minionPool.Count) minion.SetChance(chance+120);
		// ---------------------------------------------
		// Which minion will be moved in next?
		// Case 3 : If a minions is in advantage - 60%
		// higher ATK value - 30%
		// check the nearest enemy's ATK 
		if (fieldNum == 14)
		{
			minion.SetChance (chance + 100);
			return;
		}
		if (fieldNum > 14)
		{
			for (int i = 0; i < 14; i++)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					_enemy = fieldController.fieldList [i].GetComponentInChildren (typeof (Minion)) as Minion;
					if (_enemy != null)
					{
						if (minion.GetAttack () > _enemy.GetAttack ())
						{
							minion.SetChance (chance + 60);
							// if it's close to win
							if (fieldNum <= 7)
								minion.SetChance (chance + 10);
							else if (i > 7)
								minion.SetChance (chance + 20);
						}
						else
						{
							minion.SetChance (chance + 30);
						}
					}

				}
			}
		}
		else
		{
			for (int i = 27; i > 14; i--)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					_enemy = fieldController.fieldList [i].GetComponentInChildren (typeof (Minion)) as Minion;
					if (_enemy != null)
					{
						if (minion.GetAttack () > _enemy.GetAttack ())
						{
							minion.SetChance (chance + 60);
							// if it's close to win
							if (fieldNum >= 21)
								minion.SetChance (chance + 10);
							else if (i < 21)
								minion.SetChance (chance + 20);
						}
						else
							minion.SetChance (chance + 30);
					}

				}
			}
		}
	}
	void UseSpell ()
	{
		// There is 30% to use spell
		int chance_Using = Random.Range (1, 101);
		if (chance_Using < 50) print ("AI is going to use Spell !!");
		else
		{
			print ("AI decided not to use Spell ...");
			return;
		}

		// 1. Check if there are any Spell in hands, store them in spellPool
		spellPool = new List<GameObject> ();
		for (int i = 0; i < hands.transform.childCount; i++)
		{
			if (hands.transform.GetChild (i).GetComponent<Spell> () != null)   // check if it is spell
				spellPool.Add (hands.transform.GetChild (i).gameObject);
		}

		// 1.1 No any spell
		if (spellPool.Count == 0)
		{
			print ("But there are no any Spell ...");
			return;
		}

		// 2. Random choose a spell from list
		chance_inSpellPool = Random.Range (0, spellPool.Count);

		FindObjectOfType<CardDisplayer> ().DisplayCard (spellPool [chance_inSpellPool].GetComponent<Card> ());

		// 3. Select the target
		// Get two of the enemy which is most front in each path
		// Get two of the minions which is most front in each path
		GameObject candidateEnemy_top = null;
		GameObject candidateEnemy_bot = null;
		GameObject candidateMinion_top = null;
		GameObject candidateMinion_bot = null;
		target_Spell = null;

		// 3.1 Top path : Enemy
		for (int i = 27; i > 14; i--)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				candidateEnemy_top = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}
		// 3.2 Bottom path : Enemy
		for (int i = 0; i < 14; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				candidateEnemy_bot = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}
		// 3.3 Top path : Minion
		for (int i = 15; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				candidateMinion_top = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}
		// 3.4 Bottom path : Minion
		for (int i = 14; i > 1; i--)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
			{
				candidateMinion_bot = fieldController.fieldList [i].transform.GetChild (0).gameObject;
			}
		}

		// 4. After assigning the minion and enemy
		// It would be 4 different situation
		// 		4.1. 2 minions, 2 enemy -> Get the most close enemy as target
		// 4.1.1 Distance in Top is shortest
		// 4.1.2 Distance in Top is shortest
		// 4.1.3 Same Distance
		// 		4.2. Debuff :
		// 4.2.1 If there is 1 or 0 minions, 2 enemy -> No need to compare, choose the only one or the random one
		// 4.2.2 If there is just 1 enemy -> No need to compare, choose the only one
		// 		4.3. Buff : 
		// 4.3.1 If there is 1 or 0 enemy, 2 minions -> No need to compare, choose the only one or the random one
		// 4.3.2 If there is just 1 minion -> No need to compare, choose the only one

		// 4.1
		if (candidateEnemy_top != null && candidateEnemy_bot != null &&
			candidateMinion_top != null && candidateMinion_bot != null)
		{
			int e_top, _e_top;
			int.TryParse (candidateEnemy_top.transform.parent.name, out _e_top);
			e_top = _e_top;

			int e_bot, _e_bot;
			int.TryParse (candidateEnemy_bot.transform.parent.name, out _e_bot);
			e_bot = _e_bot;

			int m_top, _m_top;
			int.TryParse (candidateMinion_top.transform.parent.name, out _m_top);
			m_top = _m_top;

			int m_bot, _m_bot;
			int.TryParse (candidateMinion_top.transform.parent.name, out _m_bot);
			m_bot = _m_bot;

			if (e_top - m_top > m_bot - e_bot)  // 4.1.1
			{
				if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Buff)
					target_Spell = candidateMinion_top;
				else if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff)
					target_Spell = candidateEnemy_top;
				else
					Debug.Log ("Cannot find Spell Type : 4.1.1");
			}
			else if (e_top - m_top < m_bot - e_bot) // 4.1.2
			{
				if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Buff)
					target_Spell = candidateMinion_bot;
				else if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff)
					target_Spell = candidateEnemy_bot;
				else
					Debug.Log ("Cannot find Spell Type : 4.1.2");
			}
			else    // 4.1.3
			{
				int r = Random.Range (1, 101);
				target_Spell = (r < 50) ? candidateEnemy_top : candidateEnemy_bot;
			}
		}
		// 4.2
		else if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff)
		{   // 4.2.1
			if (candidateEnemy_top != null && candidateEnemy_bot != null &&
				((candidateMinion_top != null || candidateMinion_bot != null) ||
				(candidateMinion_top == null && candidateMinion_bot == null)))
			{
				if (candidateMinion_top != null)
				{
					target_Spell = candidateEnemy_top;
				}
				else if (candidateMinion_bot != null)
				{
					target_Spell = candidateEnemy_bot;
				}
				else if (candidateMinion_top == null && candidateMinion_bot == null)
				{
					int r = Random.Range (1, 101);
					target_Spell = (r < 50) ? candidateEnemy_top : candidateEnemy_bot;
				}
			}
			// 4.2.2
			else if (candidateEnemy_top != null || candidateEnemy_bot != null)
			{
				if (candidateEnemy_top != null)
				{
					target_Spell = candidateEnemy_top;
				}
				else if (candidateEnemy_bot != null)
				{
					target_Spell = candidateEnemy_bot;
				}
			}
			else return;
		}
		// 4.3
		else if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Buff)
		{   // 4.3.1
			if (candidateMinion_top != null && candidateMinion_bot != null &&
				((candidateEnemy_top != null || candidateEnemy_bot != null) ||
				(candidateEnemy_top == null && candidateEnemy_bot == null)))
			{
				if (candidateEnemy_top != null)
				{
					target_Spell = candidateMinion_top;
				}
				else if (candidateEnemy_bot != null)
				{
					target_Spell = candidateMinion_bot;
				}
				else if (candidateEnemy_top == null && candidateEnemy_bot == null)
				{
					int r = Random.Range (1, 101);
					target_Spell = (r < 50) ? candidateMinion_top : candidateMinion_bot;
				}
			}
			// 4.3.2
			else if (candidateMinion_top != null || candidateMinion_bot != null)
			{
				if (candidateMinion_top != null)
				{
					target_Spell = candidateMinion_top;
				}
				else if (candidateMinion_bot != null)
				{
					target_Spell = candidateMinion_bot;
				}
			}
			else return;
		}

		// 5. Spell moving anim, It will turn on the SpellAction() in Update()
		//		than activate the spell.
		isSpellMoving = true;
		isSpellEffectCompleted = false;

	}

	void SpellAction (List<GameObject> spellPool, int chance_inSpellPool, GameObject target_Spell)
	{
		if (isSpellMoving && target_Spell != null)
		{
			float speed = Time.deltaTime * 500f;
			//Debug.Log("Spell function is calling in update(). But not going into the IF");
			//Debug.Log("Starting point : " + spellPool[chance_inSpellPool].transform.position);
			//Debug.Log("Destination : " + targetEnemy_Spell.transform.position);
			//Debug.Log("Distance : " + Vector2.Distance(spellPool[chance_inSpellPool].transform.position, targetEnemy_Spell.transform.position));
			if (Vector2.Distance (spellPool [chance_inSpellPool].transform.position, target_Spell.transform.position) > 0.1f)
			{
				spellPool [chance_inSpellPool].transform.position =
					Vector2.MoveTowards (
						spellPool [chance_inSpellPool].transform.position,
						target_Spell.transform.position,
						speed);
				//Debug.Log("Spell function is calling in update().");
			}
			else
			{
				// Activate spell, Modify value
				if (!isSpellEffectCompleted)
				{
					// If it is Debuff
					if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff)
					{
						int debuffToATK = spellPool [chance_inSpellPool].GetComponent<Spell> ().GetAttack ();
						int debuffToHP = spellPool [chance_inSpellPool].GetComponent<Spell> ().GetHealth ();
						// ATK will be 0 if the spell's atk value is bigger than the target's atk value
						if (target_Spell.GetComponent<Minion> ().GetAttack () + debuffToATK <= 0)
						{
							target_Spell.GetComponent<Minion> ().SetAttack (1);
							target_Spell.transform.Find ("Text_Attack").GetComponent<Text> ().text = "1";
						}
						else
						{
							target_Spell.GetComponent<Minion> ().SetAttack (target_Spell.GetComponent<Minion> ().GetAttack () + debuffToATK);
							target_Spell.transform.Find ("Text_Attack").GetComponent<Text> ().text = target_Spell.GetComponent<Minion> ().GetAttack ().ToString ();
						}
						// Modify the Health
						target_Spell.GetComponent<Minion> ().TakeDamage (-debuffToHP);
					}
					// If it is Buff
					if (spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Buff)
					{
						int buffToATK = spellPool [chance_inSpellPool].GetComponent<Spell> ().GetAttack ();
						int buffToHP = spellPool [chance_inSpellPool].GetComponent<Spell> ().GetHealth ();

						target_Spell.GetComponent<Minion> ().SetAttack (target_Spell.GetComponent<Minion> ().GetAttack () + buffToATK);
						target_Spell.transform.Find ("Text_Attack").GetComponent<Text> ().text = target_Spell.GetComponent<Minion> ().GetAttack ().ToString ();
						target_Spell.GetComponent<Minion> ().SetHealth (target_Spell.GetComponent<Minion> ().GetHealth () + buffToHP);
						target_Spell.transform.Find ("Text_Health").GetComponent<Text> ().text = target_Spell.GetComponent<Minion> ().GetHealth ().ToString ();

					}
				}
				else
				{
					isSpellEffectCompleted = true;
				}
				isSpellMoving = false;
				Destroy (spellPool [chance_inSpellPool].gameObject);
				if(spellPool [chance_inSpellPool].GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff){
					FindObjectOfType<SoundManager> ().PlaySound ("debuffSpell");
				}else
				{
					FindObjectOfType<SoundManager> ().PlaySound ("buffSpell");
				}
			}
		}
		else if (isSpellMoving && target_Spell == null)
		{
			Destroy (spellPool [chance_inSpellPool].gameObject);
		}
	}

	void Evaluation_EntryMPath ()
	{
		// In this function , finally it will assign a target as the moving one which will go into middle path
		// Future update : check if the current path has any enemy here
		// Yes, don't go
		// No, go 
		chosenMinion_mPathA = null;
		chosenMinion_mPathB = null;
		// 1. check if the middle path has our minions
		// 1.1 Both mPath has minions
		if (mPathPool_A.Count != 0 && mPathPool_B.Count != 0)
		{
			// Decide if go in the mPath
			int randomChance = Random.Range (1, 101);
			if (randomChance < 100)
			{
				// Decide which one to be chosen
				randomChance = Random.Range (1, 101);
				if (randomChance < 50)
				{
					chosenMinion_mPathA = mPathPool_A [mPathPool_A.Count - 1]; // get the last one as the target
				}
				else
				{
					chosenMinion_mPathB = mPathPool_B [mPathPool_B.Count - 1]; // get the last one as the target
				}
			}
			else
			{
				print ("Oh, AI decided not going into the MPath");
			}
		}
		// 1.2 There is just one path has minions
		else if (mPathPool_A.Count != 0 || mPathPool_B.Count != 0)
		{
			int randomChance = Random.Range (1, 101);
			if (randomChance < 100)
			{
				if (mPathPool_A.Count != 0)
				{
					randomChance = Random.Range (1, 101);
					if (randomChance < 100)
					{
						chosenMinion_mPathA = mPathPool_A [mPathPool_A.Count - 1]; // get the last one as the target
					}
					else
					{
						chosenMinion_mPathA = null;
					}
				}
				else if (mPathPool_B.Count != 0)
				{
					randomChance = Random.Range (1, 101);
					if (randomChance < 100)
					{
						chosenMinion_mPathB = mPathPool_B [mPathPool_B.Count - 1]; // get the last one as the target
					}
					else
					{
						chosenMinion_mPathB = null;
					}
				}
			}
			else
			{
				print ("Oh, AI decided not going into the MPath");
			}

		}
		// 1.3 No any minions in both mPath
		else
		{
			// than we consider about the minions in 10 / 18
			int randomChance = Random.Range (1, 101);
			if (randomChance < 100)
			{
				for (int i = 0; i < fieldController.fieldList.Count; i++)
				{
					// Check for middle path, store the potential minions
					if (i == 10)
					{
						if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
						{
							chosenMinion_mPathA = fieldController.fieldList [i].transform.GetChild (0).gameObject;
						}
					}
					else if (i == 18)
					{
						if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 1)
						{
							chosenMinion_mPathB = fieldController.fieldList [i].transform.GetChild (0).gameObject;
						}
					}
				}
				if (chosenMinion_mPathA != null && chosenMinion_mPathB != null)
				{
					randomChance = Random.Range (1, 101);
					if (randomChance < 100)
					{
						chosenMinion_mPathA = null;
					}
					else
					{
						chosenMinion_mPathB = null;
					}
				}
			}
			else
			{
				print ("Oh, AI decided not going into the MPath");
			}

			// Up to here, we have choose the only minion which is not null for the next computation.
			// if A and B are both still null here, which means there is no any potential or existing minions

		}
	}

	GameObject CheckMpathStep (GameObject chosenMinion_mPath)
	{
		// 1. Check where is the chosenMinion_mPath now
		// 2. Calculate how many time can move
		// 1.1 If it is in 10/18
		_step_mPath = 0;
		if (chosenMinion_mPath.transform.parent.name == "10")
		{
			for (int i = 4; i > 4 - ButtonManager.dice_number; i--)
			{
				if (i >= 0)
				{
					if (fieldController.mPathList_A [4 - i].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = 4 - i;
						if (_step_mPath == 0)
							return targetedfield_mPath = null;  // something blocked at first step
						return targetedfield_mPath = fieldController.mPathList_A [4 - i - 1].gameObject;
					}
					if (i == 4 - ButtonManager.dice_number + 1)
					{
						_step_mPath = 5 - i;
						return targetedfield_mPath = fieldController.mPathList_A [4 - i].gameObject;
					}
				}
				else if (i < 0)
				{
					if (fieldController.fieldList [4].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = 5;
						return targetedfield_mPath = fieldController.mPathList_A [4].gameObject;
					}
					else
					{
						_step_mPath = 6;
						return targetedfield_mPath = fieldController.fieldList [4].gameObject;
					}
				}
			}
		}
		else if (chosenMinion_mPath.transform.parent.name == "18")
		{
			for (int i = 0; i < ButtonManager.dice_number; i++)
			{
				if (i <= 4)
				{
					if (fieldController.mPathList_B [i].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = i;
						if (_step_mPath == 0)
							return targetedfield_mPath = null; // something blocked at first step
						return targetedfield_mPath = fieldController.mPathList_B [i - 1].gameObject;
					}
					if (i == ButtonManager.dice_number + 1)
					{
						_step_mPath = i;
						return targetedfield_mPath = fieldController.mPathList_B [i].gameObject;
					}
				}
				else if (i > 4)
				{
					if (fieldController.fieldList [24].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = 5;
						return targetedfield_mPath = fieldController.mPathList_B [4].gameObject;
					}
					else
					{
						_step_mPath = 6;
						return targetedfield_mPath = fieldController.fieldList [24].gameObject;
					}
				}
			}
		}
		else Debug.Log ("I am not in any if.....");
		// 1.2 If it is in the path
		//name, not array index
		int mPathName = 0;
		for (int i = 4; i >= 0; i--)
		{
			if (chosenMinion_mPath.transform.parent.name == "a" + i)
			{
				mPathName = i;
				if (mPathName > 0)
				{
					for (int j = ((mPathName - 4) * -1) + 1; j < ((mPathName - 4) * -1) + ButtonManager.dice_number; j++)
					{
						if (j > 4)
						{
							if (fieldController.fieldList [4].GetComponent<DropOnArea> ().hasMinionCard != -1)
							{
								_step_mPath = 4 - ((mPathName - 4) * -1);
								mPathIndex = ((mPathName - 4) * -1);
								return targetedfield_mPath = fieldController.mPathList_A [4].gameObject;
							}
							else
							{
								_step_mPath = 5 - ((mPathName - 4) * -1);
								mPathIndex = ((mPathName - 4) * -1);
								return targetedfield_mPath = fieldController.fieldList [4].gameObject;
							}

						}
						// check if next field is empty
						if (fieldController.mPathList_A [j].GetComponent<DropOnArea> ().hasMinionCard != -1)
						{
							_step_mPath = j - ((mPathName - 4) * -1) - 1;
							if (_step_mPath == 0)
								return null;
							mPathIndex = ((mPathName - 4) * -1);
							return targetedfield_mPath = fieldController.mPathList_A [j - 1].gameObject;
						}
					}
				}
				// a0, next index is out of the path
				else
				{
					if (fieldController.fieldList [4].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = 0;
						return null;
					}
					else
					{
						_step_mPath = 1;
						mPathIndex = 4;
						return targetedfield_mPath = fieldController.fieldList [4].gameObject;
					}
				}

			}
		}
		for (int i = 0; i <= 4; i++)
		{
			if (chosenMinion_mPath.transform.parent.name == "b" + i)
			{
				mPathName = i;
				if (mPathName > 4)
				{
					for (int j = mPathName + 1; j < mPathName + ButtonManager.dice_number; j++)
					{
						if (j > 4)
						{
							if (fieldController.fieldList [24].GetComponent<DropOnArea> ().hasMinionCard != -1)
							{
								_step_mPath = 4 - mPathName;
								mPathIndex = mPathName;
								return targetedfield_mPath = fieldController.mPathList_B [4].gameObject;
							}
							else
							{
								_step_mPath = 5 - mPathName;
								mPathIndex = mPathName;
								return targetedfield_mPath = fieldController.fieldList [24].gameObject;
							}
						}
						// check if next field is empty
						if (fieldController.mPathList_B [j].GetComponent<DropOnArea> ().hasMinionCard != -1)
						{
							_step_mPath = j - mPathName - 1;
							if (_step_mPath == 0)
								return null;
							mPathIndex = mPathName;
							return targetedfield_mPath = fieldController.mPathList_B [j - 1].gameObject;
						}
					}
				}
				// b4, next index is out of the path
				else
				{
					if (fieldController.fieldList [24].GetComponent<DropOnArea> ().hasMinionCard != -1)
					{
						_step_mPath = 0;
						return null;
					}
					else
					{
						_step_mPath = 1;
						mPathIndex = 4;
						return targetedfield_mPath = fieldController.fieldList [24].gameObject;
					}
				}
			}
		}

		return targetedfield_mPath;
	}

	private void Waiting()
	{
		// Do nothing
	}
}
