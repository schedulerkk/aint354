﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldController : MonoBehaviour
{
	[HideInInspector] public GameObject _targetField = null;
	[HideInInspector] public List<GameObject> fieldList = new List<GameObject> ();
	int numOfField = 28;

	public GameObject [] middleFieldsA = new GameObject [5];
	public GameObject [] middleFieldsB = new GameObject [5];

	//	Middle Path for AI
	[HideInInspector] public GameObject _mPathField = null;
	[HideInInspector] public List<GameObject> mPathList_A = new List<GameObject>(); 
	[HideInInspector] public List<GameObject> mPathList_B = new List<GameObject>(); 

	void Start ()
	{
		for (int i = 0; i < numOfField; i++)
		{
			_targetField = GameObject.Find (i + "");
			if (_targetField != null)
			{
				fieldList.Add (_targetField);
			}
		}

		for(int i=4;i>=0;i--)
		{
			_mPathField = GameObject.Find("a" + i);
			if(_mPathField != null)
			{
				mPathList_A.Add(_mPathField);
			}
		}

		_mPathField = null;

		for(int i=0;i<5;i++)
		{
			_mPathField = GameObject.Find("b" + i);
			if(_mPathField != null)
			{
				mPathList_B.Add(_mPathField);
			}
		}

	}
}
