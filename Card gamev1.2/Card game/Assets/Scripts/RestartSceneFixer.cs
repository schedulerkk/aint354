﻿using UnityEngine;
using UnityEngine.SceneManagement;

// Referenced from https://answers.unity.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
public class RestartSceneFixer : MonoBehaviour
{

	void OnEnable ()
	{
		//Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable ()
	{
		//Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading (Scene scene, LoadSceneMode mode)
	{
		ButtonManager.dice_number = 1;

		GameFlowController.playerTurn = 0;
		GameFlowController.currentRound = 1;
		GameFlowController.isRolled = false;
		GameFlowController.isMoving = false;
		GameFlowController.isMoved = false;
		GameFlowController.isGameOver = false;

		MinionArrowMovement.arrowsDisplaying = false;
		MinionArrowMovement.frontArrow = null;
		MinionArrowMovement.backArrow = null;
		MinionArrowMovement.middleArrow = null;
		MinionArrowMovement.createArrows = false;
		MinionArrowMovement.currentSelectedMinion = null;

		AI.forward = false;
		AI.hasAIAlready = false;

		DevelopmentMode.minionMoveMode = DevelopmentMode.MinionMoveMode.Arrow;
	}
}
