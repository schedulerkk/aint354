﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class GameFlowController : MonoBehaviour
{
	// Variables
	public static int playerTurn;       // 0-player, 1-AI
	public static int currentRound;
	public static bool isRolled;
	public static bool isMoving;		// Prevent player from moving multiple minions when the first minion is moving
	public static bool isMoved;
	public static bool isGameOver;

	//public static BasicAI AI;

	[Header ("References")]
	[SerializeField] private GameObject gameRoundBackground;
	[SerializeField] private GameObject gameOverPanel;
	[SerializeField] private Button diceButton;
	[SerializeField] private Animator diceGlow;
	[SerializeField] private Button endTurnButton;

	private void Awake ()
	{
		isGameOver = false;
	}

	void Start ()
	{
		playerTurn = 0;
		currentRound = 1;
		//AI = gameObject.AddComponent<BasicAI>();
		isRolled = false;
		isMoving = false;
		isMoved = false;

		Debug.Log ("It's player " + playerTurn + "'s turn now.");
	}

	public void StartRound ()
	{
		// - Keep track of player actions and toggle buttons for better game-play experience -
		CancelInvoke ("ToggleButtons");
		if (playerTurn == 0)
		{
			InvokeRepeating ("ToggleButtons", 0.1f, 0.3f);
		}

		// - Prompt the player whose round is the current round -
		PromptCurrentRound ();

		// Draw a new card for the player
		GetComponent<CardDrawer> ().DrawCard (playerTurn);

		if (playerTurn == 0)
		{
			ToggleDragInPlayerHand (true);
		}
		else
		{
			ToggleDragInPlayerHand (false);
			StartCoroutine (GetComponent<AI> ().RunAI ());
		}
	}

	private void ToggleButtons ()
	{
		if (!CheckMinionOnField () || isRolled)
		{
			diceButton.interactable = false;
			diceGlow.SetBool ("isGlowing", false);
		}
		else
		{
			diceButton.interactable = true;
			diceGlow.SetBool ("isGlowing", true);
		}

		// Error Handling - The player can only end his round when he has no minion on field & on his hands
		if (!CheckMinionOnField () &&
			GameObject.Find ("Panel_Hand_0").GetComponentsInChildren<Minion> ().Length <= 0 ||
			isRolled && isMoved)
		{
			endTurnButton.interactable = true;
			GameObject.Find ("EndTurnButton").GetComponent<Animator> ().SetBool ("isGlowing", true);
			CancelInvoke ("ToggleButtons");
		}
		else
		{
			endTurnButton.interactable = false;
			GameObject.Find ("EndTurnButton").GetComponent<Animator> ().SetBool ("isGlowing", false);
		}
	}

	/*void Update ()
	{
		if (playerTurn == 1)
		{
			// Turn is switched in ButtonManager.
			// If it is AI's turn.
			// Run AI logic here
			//AI.RunAI ();
		}
		if (playerTurn == 0)
		{

		}
	}*/

	// Determines if the card belongs to the player, player cannot move AI's card
	public static void cardRightControll ()
	{
		GameObject [] cards = GameObject.FindGameObjectsWithTag ("Card");

		foreach (GameObject c in cards)
		{
			if (c.GetComponent<Card> ().cardRef != 0 ||
					(DevelopmentMode.minionMoveMode != DevelopmentMode.MinionMoveMode.DragNDrop &&
					(!c.transform.parent.name.Equals ("Panel_Hand_0") && !c.transform.parent.name.Equals ("Panel_Hand_1"))))
			{
				c.GetComponent<Drag> ().enabled = false;
			}
			else
			{
				if (playerTurn == 0)
					c.GetComponent<Drag> ().enabled = true;
			}
		}
	}

	// Determines whether the player can move his cards (in field) in various circumstances
	public static void PlayerCardRightInField ()
	{
		FieldController fieldController = FindObjectOfType<FieldController> ();

		// Dice has not been rolled yet
		if (!isRolled)
		{
			for (int i = 0; i < fieldController.fieldList.Count; i++)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					GameObject minionCard = fieldController.fieldList [i].transform.Find ("MinionCard(Clone)").gameObject;

					minionCard.GetComponent<Drag> ().enabled = false;
					minionCard.GetComponent<Animator> ().SetBool ("isGlowing", false);
				}
			}
		}
		else		// Dice rolled
		{
			for (int i = 0; i < fieldController.fieldList.Count; i++)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					GameObject minionCard = fieldController.fieldList [i].transform.Find ("MinionCard(Clone)").gameObject;

					if (DevelopmentMode.minionMoveMode == DevelopmentMode.MinionMoveMode.DragNDrop)
					{
						minionCard.GetComponent<Drag> ().enabled = true;
						minionCard.GetComponent<Animator> ().SetBool ("isGlowing", true);
					}
				}
			}
		}

		// Dice rolled but not yet make a move
		if (isRolled && !isMoving && !isMoved)
		{
			for (int i = 0; i < fieldController.fieldList.Count; i++)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					GameObject minionCard = fieldController.fieldList [i].transform.Find ("MinionCard(Clone)").gameObject;

					if (DevelopmentMode.minionMoveMode == DevelopmentMode.MinionMoveMode.DragNDrop)
					{
						minionCard.GetComponent<Drag> ().enabled = true;
					}
					minionCard.GetComponent<Animator> ().SetBool ("isGlowing", true);
				}
			}
		}
		else
		{
			for (int i = 0; i < fieldController.fieldList.Count; i++)
			{
				if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
				{
					GameObject minionCard = fieldController.fieldList [i].transform.Find ("MinionCard(Clone)").gameObject;

					minionCard.GetComponent<Drag> ().enabled = false;
					minionCard.GetComponent<Animator> ().SetBool ("isGlowing", false);
				}
			}
		}
	}

	// Toggle whether the player can drag card while in hand
	public static void ToggleDragInPlayerHand (bool enable)
	{
		foreach (Transform child in GameObject.Find ("Panel_Hand_0").transform)
		{
			if (child.tag == "Card")
			{
				child.GetComponent<Drag> ().enabled = enable;
				Debug.Log("Name : " + child.name + child.GetComponent<Drag> ().enabled);
				// - Make the minions glow -
				child.GetComponent<Animator> ().SetBool ("isGlowing", false);
				if (enable)
				{
					// Check if the player's first field has not been occupied
					if (FindObjectOfType<FieldController> ().fieldList [0].GetComponent<DropOnArea> ().hasMinionCard == -1 && 
						child.GetComponent<Card>().GetCardType() == Card.CardType.Minion ||
						child.GetComponent<Card> ().GetCardType () == Card.CardType.Spell && CheckMinionOnField ())
					{
						child.GetComponent<Animator> ().SetBool ("isGlowing", true);
					}
				}
			}
		}
	}

	public static bool CheckMinionOnField ()
	{
		List<GameObject> minionPool = new List<GameObject> ();
		FieldController fieldController = FindObjectOfType<FieldController> ();

		for (int i = 0; i < fieldController.fieldList.Count; i++)
		{
			if (fieldController.fieldList [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				return true;
			}
		}
		for (int i = 0; i < fieldController.middleFieldsA.Length; i++)
		{
			if (fieldController.middleFieldsA [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				return true;
			}
		}
		for (int i = 0; i < fieldController.middleFieldsB.Length; i++)
		{
			if (fieldController.middleFieldsB [i].GetComponent<DropOnArea> ().hasMinionCard == 0)
			{
				return true;
			}
		}

		return false;
	}

	private void PromptCurrentRound ()
	{
		gameRoundBackground.transform.Find ("Text_Round").GetComponent<TextMeshProUGUI> ().text = "[ Round " + currentRound + " ]";
		gameRoundBackground.transform.Find ("Text_Turn").GetComponent<TextMeshProUGUI> ().text = (playerTurn == 0 ? "Player" : "AI") + "'s Turn";
		gameRoundBackground.SetActive (true);
		Invoke ("DisableGameRoundText", 1.4f);

		// Play audio effect
		FindObjectOfType<SoundManager> ().PlaySound ("newRound");
	}

	private void DisableGameRoundText ()
	{
		gameRoundBackground.SetActive (false);
	}

	// Decides which player has won the game
	public void WinGame (int player)
	{
		isGameOver = true;

		gameOverPanel.SetActive (true);

		Animator anim = gameOverPanel.GetComponent<Animator> ();

		if (player == 0)        // Player wins
		{
			gameOverPanel.transform.Find ("Title_Win").gameObject.SetActive (true);
			anim.SetBool ("hasWon", true);
			FindObjectOfType<SoundManager> ().PlaySound ("win");
		}
		else        // AI wins
		{
			gameOverPanel.transform.Find ("Title_Lose").gameObject.SetActive (true);
			anim.SetBool ("hasLost", true);
			FindObjectOfType<SoundManager> ().PlaySound ("lose");
		}
	}
}
