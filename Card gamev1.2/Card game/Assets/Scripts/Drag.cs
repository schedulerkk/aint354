﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	GameObject cardPlace = null;

	List<RaycastResult> raycastResults = new List<RaycastResult> ();

	void Start ()
	{
		Card c = this.GetComponent<Card> ();

		c.owner = this.transform.parent;

		Transform _owner = c.owner;

		string _cardRef = string.Empty;
		for (int i = 0; i < _owner.name.Length; i++)
		{
			if (Char.IsDigit (_owner.name [i]))
				_cardRef += _owner.name [i];
		}

		if (_cardRef.Length > 0 && c != null)
			c.cardRef = int.Parse (_cardRef);
		//Debug.Log("My card name is " + this.name + " ,I am belong to player " + cardRef);
	}

	public void OnBeginDrag (PointerEventData ped)
	{
		//Debug.Log ("Begin Drag");

		cardPlace = new GameObject ();
		cardPlace.transform.SetParent (this.transform.parent);

		LayoutElement le = cardPlace.AddComponent<LayoutElement> ();
		le.preferredHeight = this.GetComponent<LayoutElement> ().preferredHeight;
		le.preferredWidth = this.GetComponent<LayoutElement> ().preferredWidth;
		le.flexibleHeight = 0;
		le.flexibleWidth = 0;

		cardPlace.transform.SetSiblingIndex (this.transform.GetSiblingIndex ());

		this.GetComponent<Card> ().previousParent = this.transform.parent;
		this.GetComponent<Card> ().parentReturnto = this.transform.parent;  // Record down the parent now in case of invalid drag
		this.GetComponent<Card> ().cardPlaceParent = this.GetComponent<Card> ().parentReturnto;
		this.transform.SetParent (FindObjectOfType<Canvas> ().transform);

		// Specify the targeted field
		if (this.GetComponent<Card> ().parentReturnto.name != this.GetComponent<Card> ().owner.name
			&& ButtonManager.dice_number != 0)       // Move card already in field
		{
			if (DevelopmentMode.minionMoveMode != DevelopmentMode.MinionMoveMode.DragNDrop)
				return;

			CalculateMovement ();
		}
		else        // Drag minion to spawn in field
		{
			if (this.GetComponent<Card> ().GetCardType () == Card.CardType.Minion)
			{
				int _cardRef = this.GetComponent<Minion> ().cardRef;

				if (_cardRef == 0)
				{
					this.GetComponent<Minion> ().targetedfield_1 = this.GetComponent<Minion> ().fieldController.fieldList [0];
					this.GetComponent<Minion> ().targetedfield_2 = this.GetComponent<Minion> ().targetedfield_1;
				}
				else if (_cardRef == 1)
				{
					this.GetComponent<Minion> ().targetedfield_1 = this.GetComponent<Minion> ().fieldController.fieldList [14];
					this.GetComponent<Minion> ().targetedfield_2 = this.GetComponent<Minion> ().targetedfield_1;
				}
			}
			// Here (targetedfield_2 can't be NULL): this.GetComponent<Card> ().targetedfield_2 = null;

		}

		GetComponent<LayoutElement> ().enabled = true;
		GetComponent<CanvasGroup> ().blocksRaycasts = false;    // So the card won't block the ray when dragging
	}

	public void OnDrag (PointerEventData ped)
	{
		//Debug.Log("Dragging");

		this.transform.position = ped.position;

		// Set parent for determining where the card is going to
		Transform _cardPlaceParent = this.GetComponent<Card> ().cardPlaceParent;
		if (cardPlace.transform.parent != _cardPlaceParent)
		{
			cardPlace.transform.SetParent (_cardPlaceParent);
		}

		if (this.GetComponent<Card> ().GetCardType () == Card.CardType.Minion)
		{
			Minion m = this.GetComponent<Minion> ();

			// Show avaliable field
			GameObject _targetedfield_1 = m.targetedfield_1;
			GameObject _targetedfield_2 = m.targetedfield_2;

			// Reset field colors
			if (_targetedfield_1 != null)
				_targetedfield_1.GetComponent<Image> ().color = Color.green;
			if (_targetedfield_2 != null)
				_targetedfield_2.GetComponent<Image> ().color = Color.green;

			// ---
			int newSiblingIndex = _cardPlaceParent.childCount;

			for (int i = 0; i < _cardPlaceParent.childCount; i++)
			{
				if (this.transform.position.x < _cardPlaceParent.GetChild (i).position.x)
				{
					newSiblingIndex = i;
					if (cardPlace.transform.GetSiblingIndex () < newSiblingIndex)
					{
						newSiblingIndex--;
					}
					break;
				}
			}
			cardPlace.transform.SetSiblingIndex (newSiblingIndex);
		}
	}

	public void OnEndDrag (PointerEventData ped)
	{
		//Debug.Log ("End Drag");
		// Check if it is card
		if(ped.pointerDrag.tag != "Card") return;

		Transform _previousParent = this.GetComponent<Card> ().previousParent;

		if (_previousParent == this.GetComponent<Card> ().owner
				&& this.GetComponent<Card> ().parentReturnto != this.GetComponent<Card> ().owner)
			GameFlowController.isRolled = true;

		// Check if an action is already taken in this round, if yes, return the card
		if (GameFlowController.isMoved && GetComponent<Card> ().GetCardType () != Card.CardType.Spell)
			this.GetComponent<Minion> ().parentReturnto = _previousParent;

		// Minion
		if (this.GetComponent<Card> ().GetCardType () == Card.CardType.Minion)
		{
			GameObject _targetedfield_1 = this.GetComponent<Minion> ().targetedfield_1;
			GameObject _targetedfield_2 = this.GetComponent<Minion> ().targetedfield_2;

			// - Reset field color -
			if (_targetedfield_1 != null)
				_targetedfield_1.GetComponent<Image> ().color = Color.white;
			if (_targetedfield_2 != null)
				_targetedfield_2.GetComponent<Image> ().color = Color.white;

			//Debug.Log (GameFlowController.isMoved);

			this.transform.SetParent (FindObjectOfType<Canvas> ().transform);
			this.transform.SetParent (this.GetComponent<Card> ().parentReturnto);      //Go or Go back to original parent
			this.transform.SetSiblingIndex (cardPlace.transform.GetSiblingIndex ());
			Minion minion = ped.pointerDrag.GetComponent<Minion> ();

			Debug.Log ("previousParent : " + _previousParent);
			Debug.Log ("parentReturnto : " + this.GetComponent<Minion> ().parentReturnto);

			// Update field status according to current card type
			if (_previousParent != this.GetComponent<Minion> ().parentReturnto)
			{
				Debug.Log ("Card ( " + this.gameObject.name + " ) moved from field ( " + _previousParent.name +
							" ) to field ( " + this.GetComponent<Minion> ().parentReturnto.name + " )");

				_previousParent.GetComponent<DropOnArea> ().hasMinionCard = -1;
				this.GetComponent<Minion> ().parentReturnto.GetComponent<DropOnArea> ().hasMinionCard = GameFlowController.playerTurn;
				minion = ped.pointerDrag.GetComponent<Minion> ();

				minion.Attack (true);
				minion.Attack (false);

				FindObjectOfType<CardDisplayer> ().DisplayCard (GetComponent<Card> ());

				GameFlowController.isMoved = true;
				GameFlowController.PlayerCardRightInField ();
				GameFlowController.ToggleDragInPlayerHand (false);
			}
			else if (_previousParent == this.GetComponent<Minion> ().parentReturnto && (this.GetComponent<Minion> ().parentReturnto == _targetedfield_1 || this.GetComponent<Minion> ().parentReturnto == _targetedfield_2))
			{
				minion.Attack (true);
				minion.Attack (false);
				GameFlowController.isMoved = true;
				GameFlowController.PlayerCardRightInField ();
				GameFlowController.ToggleDragInPlayerHand (false);
			}

			GetComponent<CanvasGroup> ().blocksRaycasts = true;     // The card can be selected right now

			EventSystem.current.RaycastAll (ped, raycastResults);       // Get all object underneath the dragging card

			if (raycastResults.Count > 0)
			{
				foreach (RaycastResult raycastResult in raycastResults)
				{
					//Debug.Log (raycastResult.gameObject.name);
				}
			}

			// - Disable 'Drag' component if movement mode is not set to drag and drop (after card has been dragged to spawn field initially) -
			if (DevelopmentMode.minionMoveMode != DevelopmentMode.MinionMoveMode.DragNDrop)
			{
				FieldController _fieldController = FindObjectOfType<FieldController> ();
				Minion mComponent = this.GetComponent<Minion> ();

				if (!mComponent.inField && mComponent.parentReturnto == _fieldController.fieldList [0].transform)
				{
					mComponent.inField = true;
					this.enabled = false;
				}
			}
		}
		else if (this.GetComponent<Card> ().GetCardType () == Card.CardType.Spell)
		{
			Transform field = this.GetComponent<Card> ().cardPlaceParent;

			// - Check for invalid dragging (i.e. not a field) -
			bool invalidDrag = true;

			FieldController fieldController = FindObjectOfType<FieldController> ();
			for (int i = 0; i < fieldController.fieldList.Count; i++)
			{
				if (field.name == fieldController.fieldList [i].name)
				{
					invalidDrag = false;
					break;
				}
			}
			for (int i = 0; i < fieldController.middleFieldsA.Length; i++)
			{
				if (field.name == fieldController.middleFieldsA [i].name)
				{
					invalidDrag = false;
					break;
				}
			}
			for (int i = 0; i < fieldController.middleFieldsB.Length; i++)
			{
				if (field.name == fieldController.middleFieldsB [i].name)
				{
					invalidDrag = false;
					break;
				}
			}

			// Also check if the target field has a minion on it (minion+spell = 2), no matter it is an enemy or not
			if (!invalidDrag && field.childCount > 1)
			{
				//Debug.Log ("Applying spell effect to field " + field);

				Transform targetCard = field.GetChild (0);
				Minion targetMinion = targetCard.GetComponent<Minion> ();

				// -- Attack --
				int spellAttack = gameObject.GetComponent<Card> ().GetAttack ();
				int newAttack = targetMinion.GetComponent<Card> ().GetAttack () + spellAttack;
				newAttack = Mathf.Clamp (newAttack, 1, 100);
				targetMinion.SetAttack (newAttack);
				// Update minion attribute text
				targetMinion.transform.Find ("Text_Attack").GetComponent<Text> ().text = targetMinion.GetAttack ().ToString ();

				// -- Health --
				int spellHealth = gameObject.GetComponent<Card> ().GetHealth ();

				if (gameObject.GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Buff)
				{
					int newHealth = targetMinion.GetHealth () + spellHealth;
					targetMinion.SetHealth (newHealth);

					// Update minion attribute text
					targetMinion.transform.Find ("Text_Health").GetComponent<Text> ().text = targetMinion.GetHealth ().ToString ();

					// Play audio effect
					FindObjectOfType<SoundManager> ().PlaySound ("buffSpell");
				}
				else if (gameObject.GetComponent<Spell> ().GetSpellType () == Spell.SpellType.Debuff)
				{
					targetMinion.TakeDamage (-spellHealth);     // Damage, update UI, destroy minion if HP reaches 0, AII In One

					// Play audio effect
					FindObjectOfType<SoundManager> ().PlaySound ("debuffSpell");
				}

				FindObjectOfType<CardDisplayer> ().DisplayCard (GetComponent<Card> ());

				// Destroy spell card after use
				Destroy (gameObject);
			}
			else
			{
				this.transform.SetParent (FindObjectOfType<Canvas> ().transform);
				this.transform.SetParent (this.GetComponent<Card> ().parentReturnto);
				this.transform.SetSiblingIndex (cardPlace.transform.GetSiblingIndex ());
				GetComponent<CanvasGroup> ().blocksRaycasts = true;     // The card can be selected right now
			}
		}

		Destroy (cardPlace);
		//Debug.Log(this.name + " 's owner is" + owner.name);
	}

	private void CalculateMovement ()
	{
		this.GetComponent<Minion> ().targetedfield_1 = null;
		this.GetComponent<Minion> ().targetedfield_2 = null;
		string fieldString = this.GetComponent<Minion> ().parentReturnto.name;
		int _fieldNum, fieldNum;
		int stepForward = ButtonManager.dice_number;
		int stepBackward = ButtonManager.dice_number;

		int.TryParse (fieldString, out _fieldNum);
		fieldNum = _fieldNum;       // Original location

		// -- Check if there is anything blocking the field --
		FieldController _fieldController = this.GetComponent<Minion> ().fieldController;

		// - Move Forward -
		for (int i = 1; i <= ButtonManager.dice_number; i++)
		{
			if (fieldNum + i > _fieldController.fieldList.Count - 1)
			{
				if (_fieldController.fieldList [fieldNum + i - _fieldController.fieldList.Count].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					_fieldController.fieldList [fieldNum + i - _fieldController.fieldList.Count].GetComponent<DropOnArea> ().hasMinionCard == 1)
				{
					stepForward = i - 1;
					break;
				}
			}
			else
			{
				if (_fieldController.fieldList [fieldNum + i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					_fieldController.fieldList [fieldNum + i].GetComponent<DropOnArea> ().hasMinionCard == 1)
				{
					stepForward = i - 1;
					break;
				}
			}
		}

		// - Move Backward -
		for (int i = 1; i <= ButtonManager.dice_number; i++)
		{
			if (fieldNum - i < 0)
			{
				if (_fieldController.fieldList [_fieldController.fieldList.Count - i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					_fieldController.fieldList [_fieldController.fieldList.Count - i].GetComponent<DropOnArea> ().hasMinionCard == 1)
				{
					stepBackward = i - 1;
					break;
				}
			}
			else
			{
				if (_fieldController.fieldList [fieldNum - i].GetComponent<DropOnArea> ().hasMinionCard == 0 ||
					_fieldController.fieldList [fieldNum - i].GetComponent<DropOnArea> ().hasMinionCard == 1)
				{
					stepBackward = i - 1;
					break;
				}
			}
		}

		//Debug.Log ("forward : " + stepForward + " backward : " + stepBackward);

		// - Assign target field - 
		if (fieldNum == 0)          // Currently @ Player start position
		{
			this.GetComponent<Minion> ().targetedfield_1 = _fieldController.fieldList [stepForward];
			this.GetComponent<Minion> ().targetedfield_2 = _fieldController.fieldList [_fieldController.fieldList.Count - stepBackward];
		}
		else if (fieldNum + stepForward >= _fieldController.fieldList.Count)        // Gonna across player start position
		{
			int _temp_TargetedField = fieldNum + stepForward - _fieldController.fieldList.Count;
			this.GetComponent<Minion> ().targetedfield_1 = _fieldController.fieldList [_temp_TargetedField];
			this.GetComponent<Minion> ().targetedfield_2 = _fieldController.fieldList [fieldNum - stepBackward];
		}
		else        // All is well
		{
			this.GetComponent<Minion> ().targetedfield_1 = _fieldController.fieldList [fieldNum + stepForward];
			//Debug.Log("" + fieldNum + " " + diceNumber );
			if (fieldNum - stepBackward < 0)
			{
				int _temp_TargetedField = _fieldController.fieldList.Count + (fieldNum - stepBackward);
				this.GetComponent<Minion> ().targetedfield_2 = _fieldController.fieldList [_temp_TargetedField];
			}
			else
				this.GetComponent<Minion> ().targetedfield_2 = _fieldController.fieldList [fieldNum - stepBackward];
			//Debug.Log("" + fieldNum + " " + diceNumber );
		}
	}
}
