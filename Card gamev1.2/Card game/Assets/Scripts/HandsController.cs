﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsController : MonoBehaviour
{

	public GameObject handsPanel;

	public void PopUp ()
	{

		if (handsPanel != null)
		{
			Animator animator = handsPanel.GetComponent<Animator> ();
			if (animator != null)
			{
				bool isPopUp = animator.GetBool ("popUp");

				animator.SetBool ("popUp", !isPopUp);
			}
		}
	}
}
